// MatriceUbriaca.cpp : Defines the entry point for the console application.
//
/*Scrivere un metodo che prenda come parametro formale una matrice quadrata M di nxn interi positivi e restituisca un array A di n float in cui:
A[i] = minimo dei valori nella colonna i-esima di M, se i è pari
A[i] = media dei valori divisibili per 3 presenti nella ria i-esima di M, se i è dispari e non divisibile per 3
A[i] = A[i-2]+A[i-1] altrimenti*/

//Autore: Andrea Luciano Damico

#include "stdafx.h" //Commentare questa linea per compilare su Linux e MacOS
#include <iostream>
#include <iomanip>
#include <ctime>

using namespace std;

const int N = 10;

float * method(unsigned M[][N]) {
	float *A = new float[N];
	for (int i = 0; i < N; i++) {
		if (i % 2 == 0) {
			int minimo = INT_MAX;
			for (int j = 0; j < N; j++) {
				if (M[j][i] < minimo) {
					minimo = M[j][i];
				}
			}
			A[i] = minimo;
		}
		else if (i % 2 == 1 && i % 3 != 0) {
			int acc = 0;
			float media = 0.0;
			for (int j = 0; j < N; j++) {
				if (M[i][j] % 3 == 0) {
					media += M[i][j];
					acc++;
				}
				A[i] = media / acc;
			}
		}
		else
			A[i] = A[i - 2] + A[i - 1];
	}
	return A;
}


//Metodo non richiesto dall'esame che riempie una matrice di interi positivi
void fillMatrix(unsigned M[][N], int rows, int columns, int min = 0, int max = 9) {
	for (int r = 0; r < rows; r++)
		for (int c = 0; c < columns; c++)
			M[r][c] = static_cast<int>(rand() % (max - min)) + min;
}

//Metodo non richiesto dall'esame che stampa un array allocato dinamicamente.
template <class T>
void printArray(T *A, int length) {
	for (int i = 0; i < length; i++)
		std::cout << fixed << setprecision(3) << A[i] << std::setw(10); //setprecision and fixed are ignored if T is an integer type
	std::cout << std::endl;
}

//Metodo non richiesto dall'esame che stampa una matrice statica MxN
template <class T>
void printMatrix(T M[][N], int rows, int columns) {
	for (int r = 0; r < rows; r++) {
		for (int c = 0; c < columns; c++) {
			std::cout << M[r][c] << "  ";
		}
		std::cout << std::endl;
	}
}


//Main per testing
int main()
{
	srand(time(0));
	unsigned M[N][N];
	fillMatrix(M, N, N);
	printMatrix(M, N, N);
	float *A = method(M); //Richiamiamo il nostro metodo e stampiamo la matrice nella riga successiva.
	printArray<float>(A, N);
	cout << endl;
	system("pause");
    return 0;
}

