// PercentualePrimaConiugazione.cpp : Defines the entry point for the console application.
//

/*Scrivere un metodo che prenda in input una matrice V di stringhe contenente verbi italiani all'infinito
e restituisca la percentuale dei verbi della prima coniugazione.*/

//#include "stdafx.h"//commentare questa riga se si vuole compilare su Linux o MacOS.
#include <iostream>
#include <string>
const int RIGHE = 5;
const int COLONNE = 2;

using namespace std;

double percentVerb(string V[][COLONNE]) {
	int qty = 0; //il numero di verbi all'infinito
	for (int i = 0; i < RIGHE; i++) {
		for (int j = 0; j < COLONNE; j++) { //Ci scansioniamo tutto l'array
			int l = V[i][j].length() - 3; //riga non necessaria, ma sul foglio non mi entrava una riga lunga. Qui conserviamo la posizione da cui dobbiamo partire per estrarre la desinenza
			string con = V[i][j].substr(l, 3); //Estraiamo la desinenza
			if (con == "are") {
				qty++; //Se il verbo finisce in are, aumentiamo il contatore qty.
			}
		}
	}
	return static_cast<double>(qty) * 100 / (RIGHE*COLONNE); //restituiamo la percentuale
}

int main()
{
	string V[RIGHE][COLONNE] = { "mangiare", "bere", "dormire", "fare", "dovere", "cagare", "smerciare", "commerciare", "dare", "dire" };
	cout << "Verbi alla prima coniugazione: " << percentVerb(V) << "%." << endl; //E infatti sono il 60%, 6 su 10.
	system("pause");
	return 0;
}

