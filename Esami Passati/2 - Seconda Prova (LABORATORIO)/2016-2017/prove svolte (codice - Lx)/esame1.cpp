/*
 * 2017 - esame1.cpp
 *
 * Questo codice è il risultato di un esercitazione individuale,
 * pertanto non è stato sottoposto a revisione ed eventuale correzione.
 *
 * Per nessun motivo, il codice che segue, deve essere considerato
 * la maniera corretta di svolgere l'esercizio descritto nel file "esame1".
 *
 * Ricordo che l'output dipende dall'ambiente in cui viene avviato questo programma,
 * perciò, con alta probabilità, NON sarà identico a quello fornito nel testo d'esame.
 *
 * Versioni datate di gcc potrebbero dare errore.
 * In questo caso, usare l'argomento -std=c++14
 */
#include <iostream>
#include <iomanip>
#include <climits>
#include <cmath>
#include <sstream>

#define DIM 50

class objStream {
	protected:
		virtual void print(std::ostream &) =0;

		friend std::ostream &operator<<(std::ostream &out, auto &obj) {
			obj.print(out);

			return out;
		}
};

class C: public objStream {
	float x;

	public:
		C(float x) { this->x = x; }
		virtual double f() =0;
		float getX() const { return this->x; }
		void print(std::ostream &out) {}
};

class D: public C {
	char c;

	public:
		D(float x): C(x) {
			this->c = (char)('a'+(int)(x*1000000000) % 20);
		}

		double f() { return pow(sin( this->getX() ), 2); }
		char getC() const { return this->c; }

		void print(std::ostream &out) {
			out << "[ " <<  this->getX() << " " << this->c << " ]";
		}
};

class E: public C {
	int n;

	public:
		E(float x): C(x) {
			this->n = (int)(x*1000000000) % 10;
		}

		double f() { return pow(cos( this->getX() ), 2); }
		int getN() const { return this->n; }

		void print(std::ostream &out) {
			out << "[ " << this->getX() << " n=" << this->n << " ]";
		}
};

class A: public objStream {
	protected:
		C *obj1;
		C *obj2;

	public:
		A(float x) {
			this->obj1 = new D {x};
			this->obj2 = new E {x};
		}

		double g() const {
			return this->obj1->f() + this->obj2->f();
		}

		void print(std::ostream &out) {
			out << "Class A: " << *((D *)this->obj1) << ", " << *((E *)this->obj2)
				<< " g()=" << this->g() << "\n";
		}
};

class B: public A {
	public:
		B(float x): A(x) {}

		std::string st() const {
			std::stringstream ret;
			int nE = ((E *)this->obj2)->getN();

			ret << std::fixed << std::setprecision(1) << this->g();

			for (int i=0; i<nE; i++)
				ret << ((D *)this->obj1)->getC();

			return ret.str();
		}

		void print(std::ostream &out) {
			out << "Class B: " <<  *((D *)this->obj1) << ", " << *((E *)this->obj2)
				<< " g()=" << this->g() << " st()=" << this->st() << "\n";
		}
};

int main() {
	A *vett[DIM];
	double gSum = 0.0;
	int stSum = 0;

	srand(832748);

	for (int i=0; i<DIM; i++) {
		if (rand() % 2 == 1) {
			vett[i] = new A {(float)rand()/INT_MAX};
		} else {
			std::string tmp = "";

			vett[i] = new B {(float)rand()/INT_MAX};
			tmp = ((B *)vett[i])->st();

			for (int j=0; j<tmp.length(); j++)
				stSum += tmp[j];
		}

		gSum += vett[i]->g();

		std::cout << *vett[i] << "\n";
	}

	std::cout << "sum1 = " << std::fixed << std::setprecision(1) << gSum
				<< "  sum2 = " << stSum << "\n";

	return 0;
}
