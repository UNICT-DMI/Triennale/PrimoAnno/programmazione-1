/*
 * 2017 - esame10.cpp
 *
 * Questo codice è il risultato di un esercitazione individuale,
 * pertanto non è stato sottoposto a revisione ed eventuale correzione.
 *
 * Per nessun motivo, il codice che segue, deve essere considerato
 * la maniera corretta di svolgere l'esercizio descritto nel file "esame10".
 *
 * Ricordo che l'output dipende dall'ambiente in cui viene avviato questo programma,
 * perciò, con alta probabilità, NON sarà identico a quello fornito nel testo d'esame.
 *
 * Versioni datate di gcc potrebbero dare errore.
 * In questo caso, usare l'argomento -std=c++14
 */
#include <iostream>
#include <iomanip>
#include <cmath>
#include <cfloat>

class objStream {
	protected:
		virtual void print(std::ostream &) =0;

	public:
		friend std::ostream &operator<<(std::ostream &out, auto &obj) {
			obj.print(out);

			return out;
		}
};

class A: public objStream {
	double x;

	public:
		A(double x) { this->x = x; }
		double getX() const { return this->x; }
		bool grande() const { return (this->x > 0.5); }
		virtual double calcola() =0;
		void print(std::ostream &out) {}
};

class D: public A {
	double w;

	public:
		D(double x, double w): A(x) { this->w = w; }
		bool grande() { return (this->getX() > (0.5 - this->w)); }
		double calcola() { return this->getX()*this->w; }

		void print(std::ostream &out) {
			out << "Class D: x=" << this->getX() << "\tw=" << this->w << "\n";
		}
};

class B: public A {
	double y;

	public:
		B(double x, double y): A(x) { this->y = y; }
		double calcola(double m) { return pow(this->getX()+m, y); }

		void print(std::ostream &out) {
			out << "y=" << this->y;
		}
};

class C: public B {
	double z;

	public:
		C(double x, double y, double z): B(x, y) { this->z = z; }
		double calcola() { return pow(this->getX(), this->z); }

		void print(std::ostream &out) {
			out << "Class C: x=" << this->getX() << "\t";
			B::print(out);
			out << "\tz=" << this->z << "\n";
		}
};

int main() {
	A *vett[100];
	double calcMax = DBL_MIN;
	double cCalcSum = 0.0;
	int bigCount = 0;

	srand(1987234111);

	for (int i=0; i<100; i++) {
		double a = rand()/(double)RAND_MAX;
		double b = rand()/(double)RAND_MAX;

		if (rand() % 2 == 1) {
			double c = rand()/(double)RAND_MAX;

			vett[i] = new C {a, b, c};
			cCalcSum += ((B *)vett[i])->calcola( vett[i]->calcola() );
		} else {
			vett[i] = new D {a, b};
		}

		double calc = vett[i]->calcola();

		if (calc > calcMax)
			calcMax = calc;

		if (vett[i]->grande())
			bigCount++;

		std::cout << *vett[i];
	}

	std::cout << "\nr = 1987234111, Output: " << bigCount << " "
				<< std::fixed << std::setprecision(16) << calcMax
				<< " " << cCalcSum << "\n";

	return 0;
}
