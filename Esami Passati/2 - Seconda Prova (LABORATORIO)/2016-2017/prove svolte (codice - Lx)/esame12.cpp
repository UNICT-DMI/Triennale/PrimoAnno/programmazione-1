/*
 * 2017 - esame12.cpp
 *
 * Questo codice è il risultato di un esercitazione individuale,
 * pertanto non è stato sottoposto a revisione ed eventuale correzione.
 *
 * Per nessun motivo, il codice che segue, deve essere considerato
 * la maniera corretta di svolgere l'esercizio descritto nel file "esame12".
 *
 * Ricordo che l'output dipende dall'ambiente in cui viene avviato questo programma,
 * perciò, con alta probabilità, NON sarà identico a quello fornito nel testo d'esame.
 *
 * Versioni datate di gcc potrebbero dare errore.
 * In questo caso, usare l'argomento -std=c++14
 */
#include <iostream>
#include <sstream>

class objStream {
	protected:
		virtual void print(std::ostream &) =0;

	public:
		friend std::ostream &operator<<(std::ostream &out, auto &obj) {
			obj.print(out);

			return out;
		}
};

class A: public objStream {
	std::string x;

	public:
		A(std::string x) { this->x = x; }
		std::string getX() const { return this->x; }
		virtual std::string guess() =0;
		void print(std::ostream &out) {}

		bool doppia() {
			int cCount = 0;

			for (int i=0, len=this->x.length(); i<len; i++) {
				for (int j=i+1; j<len; j++) {

					if (this->x[i] == this->x[j])
						cCount++;

					if (cCount)
						return true;
				}
				cCount=0;
			}

			return false;
		}
};

class B: public A {
	std::string y;
	short pos;

	public:
		B(std::string s, short p): A(s.substr(0,10)) {
			this->y = s.substr(10,20);
			this->pos = p;
		}

		char estrai() { return this->y[this->pos]; }

		std::string guess() {
			std::stringstream ret;

			for (int i=0; i<10; i++) {
				ret << this->getX()[i] << this->y[i];
			}

			return ret.str();
		}

		void print(std::ostream &out) {
			out << "{ " << this->getX() << " " << this->y << ", pos:" << this->pos << " }\n";
		}
};

class C: public A {
	std::string z;

	public:
		C(std::string s): A(s.substr(0,10)) {
			this->z = s.substr(10,20);
		}

		std::string guess() { return this->getX()+this->z; }

		void print(std::ostream &out) {
			out << "{ " << this->getX() << " " << this->z << " }\n";
		}
};

int main() {
	A *vett[20];
	std::string noDbl = "";
	std::string bEstraiConcat = "";
	std::string diagConcat = "";
	char m[20][20];

	srand(333675333);

	for (int i=0; i<20; i++) {
		std::string str = "";

		for (int j=0; j<20; j++)
			str += (char)( 'a'+(rand() % 26) );

		if (rand() % 2 == 1) {
           	vett[i] = new B {str, (short)(rand() % 10)};
			bEstraiConcat += ((B *)vett[i])->estrai();
        } else {
			vett[i] = new C {str};
		}

		if (!vett[i]->doppia())
			noDbl += " " + std::to_string(i);

		std::cout << *vett[i];
	}

	// Popola matrice
	for (int i=0; i<20; i++) {
		std::string guessRet = vett[i]->guess();

		for (int j=0; j<20; j++)
			m[i][j] = guessRet[j];
	}

	// Estrai diagonale principale
	for (int i=0; i<20; i++)
		diagConcat += m[i][i];

	std::cout << "\nOggetti senza doppie: " << noDbl
				<< "\n\nPunto 1: " << bEstraiConcat
				<<"\n\nPunto 2: " << diagConcat << "\n";

	return 0;
}
