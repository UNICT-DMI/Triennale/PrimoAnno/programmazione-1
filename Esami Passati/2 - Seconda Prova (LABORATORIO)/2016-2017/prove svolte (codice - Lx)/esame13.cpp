/*
 * 2017 - esame13.cpp
 *
 * Questo codice è il risultato di un esercitazione individuale,
 * pertanto non è stato sottoposto a revisione ed eventuale correzione.
 *
 * Per nessun motivo, il codice che segue, deve essere considerato
 * la maniera corretta di svolgere l'esercizio descritto nel file "esame13".
 *
 * Ricordo che l'output dipende dall'ambiente in cui viene avviato questo programma,
 * perciò, con alta probabilità, NON sarà identico a quello fornito nel testo d'esame.
 *
 * Versioni datate di gcc potrebbero dare errore.
 * In questo caso, usare l'argomento -std=c++14
 */
#include <iostream>
#include <sstream>
#include <cfloat>

class objStream {
	protected:
		virtual void print(std::ostream &) const =0;

	public:
	    friend std::ostream &operator<<(std::ostream &out, auto &obj){
			obj.print(out);

	    	return out;
   		}
};

class A: public objStream {
	int x;

	public:
		A(int x) { this->x = x; }
		int getX() const { return this->x; }
		virtual float f(short) =0;
		void print(std::ostream &out) const {}
};

class D: public objStream {
	char x;

	public:
		D(char x) { this->x = x; }
		char getX() const { return this->x; }
		void print(std::ostream &out) const {}
};

class E: public D {
	char y;

	public:
		E(char x, char y): D(x) { this->y = y; }

		std::string coppia() const {
			std::stringstream ret;

			ret << this->getX() << this->y;

			return ret.str();
		}

		void print(std::ostream &out) {
			out << this->coppia();
		}
};

class B: public A {
	D *obj;

	public:
		B(int x, char dx, char y): A(x) { this->obj = new E {dx, y}; }
		float f(short y) { return this->getX()+y; }

		std::string f() const {
			std::stringstream ret;

			ret << ((E *)this->obj)->coppia() << this->getX();

			return ret.str();
		}

		void print(std::ostream &out) const {
			out << "x=" << this->getX() << " " << *((E *)this->obj) << "\n";
		}
};

class C: public A {
	int *a;

	public:
		C(int p): A(p) {
			a = new int[20];

			// non "tradurre" new Random(p) in questo punto

			for (int i=0; i<20; i++)
				a[i] = rand() % 256;
		}

		float f(short x) {
			int sum = 0;

			for (int i=0; i<20; i++) {
				if (a[i] < x)
					sum += a[i];
			}

			return sum/20;
		}

		float f() const { return this->a[11]; }

		void print(std::ostream &out) const {
            out << "x=" << this->getX() << " [";

			for (int i=0; i<20; i++)
				out << std::to_string(this->a[i]) << " ";

			out << "]\n";
        }
};

int main() {
	A *vett[30];
	float fSum = 0.0;
	float fCMax = FLT_MIN;
	std::string bStr = "";

	srand(43253793);

	for (int i=0; i<30; i++) {
		int a = rand() % 30;
		char c = (char)('a' + rand() % 13);

		if (rand() % 2 == 1) {
			vett[i] = new B {a, c, (char)(c + rand() % 13)};
			bStr += ((B *)vett[i])->f();

		} else {
			vett[i] = new C {a};

			float fC = ((C *)vett[i])->f();

			if (fC > fCMax)
				fCMax = fC;
		}

		fSum += vett[i]->f(100);

		std::cout << "vett[" << i << "]: " << *vett[i];
	}

	std::cout << "\nMedia=" << fSum/30 << "  Max=" << fCMax << "\n"
				<< "Str=" << bStr << "\n";

	return 0;
}
