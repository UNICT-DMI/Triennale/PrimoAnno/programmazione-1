/*
 * 2017 - esame2.cpp
 *
 * Questo codice è il risultato di un esercitazione individuale,
 * pertanto non è stato sottoposto a revisione ed eventuale correzione.
 *
 * Per nessun motivo, il codice che segue, deve essere considerato
 * la maniera corretta di svolgere l'esercizio descritto nel file "esame2".
 *
 * Ricordo che l'output dipende dall'ambiente in cui viene avviato questo programma,
 * perciò, con alta probabilità, NON sarà identico a quello fornito nel testo d'esame.
 *
 * Versioni datate di gcc potrebbero dare errore.
 * In questo caso, usare l'argomento -std=c++14
 */
#include <iostream>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include <cfloat>
#include <typeinfo>

#define X_LEN 10

class objStream {
	protected:
		virtual void print(std::ostream &) =0;

	public:
		friend std::ostream &operator<<(std::ostream &out, auto &obj) {
			obj.print(out);

			return out;
		}
};

class A: public objStream {
	float *x;

	public:
		A(float a) {
			x = new float[X_LEN];
			x[0] = a;

			for (int i=1; i<X_LEN; i++)
				x[i] = sin(100*x[i-1]);
		}

		virtual float f() const =0;
		float *getX() const { return this->x; }
		void print(std::ostream &out) {}
};

class B: public A {
	public:
		B(float a): A(a) {}

		float f() const {
			float xMax = FLT_MIN;
			float *Ax = this->getX();

			for (int i=0; i<X_LEN; i++) {
				if (Ax[i] > xMax)
					xMax = Ax[i];
			}

			return xMax;
		}

		void print(std::ostream &out) {
			std::streamsize defaultPrecision = std::cout.precision();
			float *Ax = this->getX();
			int i=0;

			out << "Class B [ ";

			while (i < X_LEN) {
				out << std::fixed << std::setprecision(2) << Ax[i] << " ";
				i++;
			}

			out << std::setprecision(defaultPrecision) << "] f()=" << this->f() << "\n";
		}
};

class C: public A {
	public:
		C(float a): A(a) {}

		float f() const {
			float *Ax = this->getX();
			float m = Ax[0];

			for (int i=1; i<X_LEN; i++)
				m += Ax[i];

			return m /= 10;
		}

		int gte(float thr) const {
			float *Ax = this->getX();
			int count = 0;

			for (int i=0; i<X_LEN; i++) {
				if (Ax[i] > thr)
					count++;
			}

			return count;
		}

		void print(std::ostream &out) {
			std::streamsize defaultPrecision = std::cout.precision();
			float *Ax = this->getX();
			int i=0;

			out << "Class C [ ";

			while (i < X_LEN) {
				out << std::fixed << std::setprecision(2) << Ax[i] << " ";
				i++;
			}

			out << std::setprecision(defaultPrecision) << "] f()=" << this->f()
						<< " gte(.7)=" << this->gte(0.7) << "\n";
		}
};

class D: public objStream {
	A *a;
	B *b;
	C *c;

	public:
		D(float seed) {
			if (seed > 0.5)
				a = new B {seed};
			else
				a = new C {seed};

			b = new B {seed/2};
			c = new C {seed/3};
		}

		float m() const {
			A *p;
			float m = 0;
			float *Ax = a->getX();
			float *Px = NULL;

			if (typeid(*a) == typeid(B))
				p = (B *)b;
			else
				p = (C *)c;

			Px = ((A *)p)->getX();

			for (int i=0; i<X_LEN; i+=2)
				m += std::max(Ax[i], Px[i]);

			return m;
		}

		float h(float thr) const {
			float ret = c->f() / b->f();

			if (typeid(*a) == typeid(C))
				ret *= ((C *)a)->gte(thr);

			return ret;
		}

		void print(std::ostream &out) {
			if (typeid(*(this->a)) == typeid(B))
				out << *((B *)this->b);
			else
				out << *((C *)this->c);

			out << *((B *)this->b) << *((C *)this->c)
				<< "\t\t\th(.7)=" << this->h(0.7) << " m()=" << this->m() << "\n\n";
		}
};

int main () {
	D *vett[50];
	float hSum = 0.0;

	srand(347537586);

	for (int i=0; i<50; i++) {
		vett[i] = new D {rand()/(float)RAND_MAX};
		hSum += vett[i]->h(0.7);

		std::cout << *vett[i];
	}

	std::cout << "Media=" << (hSum/50) << "  vett[4].m()+vett[5].m() = " << vett[4]->m()+vett[5]->m() << "\n";
	return 0;
}
