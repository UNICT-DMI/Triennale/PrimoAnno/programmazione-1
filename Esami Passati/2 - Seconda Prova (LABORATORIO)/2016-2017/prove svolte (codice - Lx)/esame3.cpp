/*
 * 2017 - esame3.cpp
 *
 * Questo codice è il risultato di un esercitazione individuale,
 * pertanto non è stato sottoposto a revisione ed eventuale correzione.
 *
 * Per nessun motivo, il codice che segue, deve essere considerato
 * la maniera corretta di svolgere l'esercizio descritto nel file "esame3".
 *
 * Ricordo che l'output dipende dall'ambiente in cui viene avviato questo programma,
 * perciò, con alta probabilità, NON sarà identico a quello fornito nel testo d'esame.
 *
 * Versioni datate di gcc potrebbero dare errore.
 * In questo caso, usare l'argomento -std=c++14
 */
#include <iostream>
#include <time.h>

class A {
	protected:
		int x;

	public:
		A(int x) { this->x = x; }
		virtual int f() =0;
};

class D {
	double z;

	public:
		D(double z) { this->z = z; }
		double getZ() const { return this->z; }
};

class B: public A {
	D *d;

	protected:
		int y;

	public:
		B(int x, int y, double z): A(x) {
			this->y = y;
			this->d = new D {z};
		}

		int f() { return x*x; }
		int f(D *d) { return this->d->getZ() + d->getZ(); }
};

class C: public B {
	public:
		C(int x, int y, double z): B(x,y,z) {}
		int f() { return y*7; }
};

int main() {
	int fSum = 0;
	int fObjSum = 0;
	int instancesOfB = 0;

    srand(6353433);

    A *vett[100];

    for (int i=0; i<100; i++) {
        int x = rand() % 10;
        int y = rand() % 100;
        int z = rand() % 30;

        if (rand() % 2 == 1 ) {
			vett[i] = new B {x, y, (double)z};
			instancesOfB++;
        } else {
			vett[i] = new C {x, y, (double)z};
		}

		D *obj = new D {(double)(rand() % 47)};

		fSum += vett[i]->f();
		fObjSum += ((B *)vett[i])->f(obj);
    }

	std::cout << "r = 6353433\tOutput: " << fSum << ", " << fObjSum << ", " << instancesOfB << "\n";

	return 0;
}
