/*
 * 2017 - esame6.cpp
 *
 * Questo codice è il risultato di un esercitazione individuale,
 * pertanto non è stato sottoposto a revisione ed eventuale correzione.
 *
 * Per nessun motivo, il codice che segue, deve essere considerato
 * la maniera corretta di svolgere l'esercizio descritto nel file "esame6".
 *
 * Ricordo che l'output dipende dall'ambiente in cui viene avviato questo programma,
 * perciò, con alta probabilità, NON sarà identico a quello fornito nel testo d'esame.
 *
 * Versioni datate di gcc potrebbero dare errore.
 * In questo caso, usare l'argomento -std=c++14
 */
#include <iostream>
#include <climits>

class A {
	int x = 0;

	public:
		A(int x) { this->x = x; }
		virtual int f(int x) =0;
};

class E {
	float coeff = 0;

	public:
		E(double c) { this->coeff = c; }
		double getCoeff() { return this->coeff; }

		int somma(int *v, int vLen) {
			int ret = 0;

			for (int i=0; i<vLen; i++)
				ret += v[i];

			return ret;
		}
};

class B: public A {
	E *e;

	public:
		B(int x, double d): A(x) {
			this->e = new E {d};
		}

		float getCoeff() { return this->e->getCoeff(); }
		E *getE() { return this->e; }
};

class C: public B {
	int *z;
	int zLen;

	public:
		C(int x, double d, int q): B(x, d) {
			z = new int[x];
			zLen = x;

			for (int i=0; i<x; i++)
				z[i] = 2*q+i+1;

		}

		int f(int x) { return this->getE()->somma(z, zLen); }
		int ultimo() { return z[this->zLen-1]; }
};

class D: public B {
	public:
		D(int x, double d): B(x, d) {}
		int f(int x) { return x + this->getCoeff() + 100; }
};

int main() {
	A *vett[100];
	int instC;
	int fSum = 0;
	int maxUltimo = INT_MIN;

	srand(7645544);

	for (int i=0; i<100; i++) {
		int x = (rand() % 10) +1;
		double d = ((rand()/(double)RAND_MAX) * 40);
		int z = rand() % 30;

		if (rand() % 2 == 1) {
			vett[i] = new C {x, d, z};

			int mU = ((C *)vett[i])->ultimo();

			if (mU > maxUltimo)
				maxUltimo = mU;

			instC++;
		} else {
			vett[i] = new D {x, d};
		}

		fSum += vett[i]->f(rand() % 50);
	}

	std::cout << "r=7645544,\tOutput: " << fSum << ", " << maxUltimo << ", " << instC << "\n";

	return 0;
}
