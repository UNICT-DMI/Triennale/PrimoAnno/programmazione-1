/*
 * 2017 - esame14.cpp
 *
 * Questo codice è il risultato di un esercitazione individuale,
 * pertanto non è stato sottoposto a revisione ed eventuale correzione.
 *
 * Per nessun motivo, il codice che segue, deve essere considerato
 * la maniera corretta di svolgere l'esercizio descritto nel file "laboratorio 1 marzo 2016".
 *
 * Ricordo che l'output dipende dall'ambiente in cui viene avviato questo programma,
 * perciò, con alta probabilità, NON sarà identico a quello fornito nel testo d'esame.
 *
 * Versioni datate di gcc potrebbero dare errore.
 * In questo caso, usare l'argomento -std=c++14
 */
#include <iostream>
#include <cmath>
#include <string>

class MyDouble {
	double x;

	public:
		MyDouble(float x) { this->x = x; }
		int trunc() { return (int)this->x; }

		bool operator>(const MyDouble &op) const { return this->x > op.x; }

		MyDouble operator^(const MyDouble &esp) {
			return MyDouble(pow(this->x, esp.x));
		}

		friend std::ostream &operator<<(std::ostream &out, const MyDouble &md) {
			return out << md.x;
		}
};

class A {
	float w;

	protected:
		static const int dim = 4;
		MyDouble *mat[dim][dim];

	public:
		A(double w) {
			this->w = w;

			for (int i=0; i<dim; i++) {
				for (int j=0; j<dim; j++) {
					mat[i][j] = new MyDouble( (w*10)+i+j );
				}
			}
		}

		virtual double f() const =0;
		float getW() const { return this->w; }
};

class B: public A {
	public:
		B(double w): A(w) {}

		double f() const {
			double ret = 0;

			for (int i=0; i<this->dim; i++)
				ret += this->mat[0][i]->trunc();

			return ret;
		}

		friend std::ostream &operator<<(std::ostream &out, const B &b) {
			out << "Class B:\t x=" << b.getW() << "\t\t\tf()=" << b.f() << "\n";

			return out;
		}
};

template <typename T>
class C: public A {
	T c;

	public:
		C(double w, T c): A(w) { this->c = c; }

		double f() const {
			double ret = 0;
			MyDouble *md = new MyDouble(5.0);

			for (int i=0; i<this->dim; i++) {
				for (int j=0; j<this->dim; j++) {
					if (this->mat[i][j] > md)
						ret++;
				}
			}

			delete md;
		}

		MyDouble f(T p) {
			if (sizeof(T) == sizeof(double))
				return MyDouble(p);
			else
				return *this->mat[0][0]^*this->mat[dim-1][dim-1];
		}

		friend std::ostream &operator<<(std::ostream &out, C<T> &c) {
			std::string tabs = "\t";

			if (sizeof(T) == sizeof(double)) {
				out << "Class C<double>: ";
			} else {
				tabs = "\t\t";

				out << "Class C<int>:\t ";
			}

			return out << "w=" << c.getW() << "\tc=" << c.c
						<< tabs << "f()=" << c.f() << " f(3)=" << c.f(3);
		}

};

int main() {
	A *vett[30];
	int fSum = 0;
	int f3CIdx = 0;

	srand(328832748);

	for (int i=0; i<30; i++) {
		double x = rand()/(double)RAND_MAX;

		rand();

		switch (rand() % 3) {
			case 0:
				vett[i] = new B(x);
				fSum += vett[i]->f();

				std::cout << *((B *)vett[i]);
				break;
			case 1:
				vett[i] = new C<double>(x, rand()/(double)RAND_MAX);
				fSum += ((C<double> *)vett[i])->f(x).trunc();

				std::cout << *((C<double> *)vett[i]) << "\n";
				break;
			case 2:
				vett[i] = new C<int>(x, (int)(x*10));
				fSum += vett[i]->f();

				if (!f3CIdx)
					f3CIdx = i;

				std::cout << *((C<int> *)vett[i]) << "\n";
				break;
			default:
				break;
		}

	}

	std::cout << "\npunto 1: sum=" << fSum << ",   punto 2: f(3)=" << ((C<int> *)vett[f3CIdx])->f(3)
				<< " di indice " << f3CIdx << "\n";

	return 0;
}
