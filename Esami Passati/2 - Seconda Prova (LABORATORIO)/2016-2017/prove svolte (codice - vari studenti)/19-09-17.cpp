
// Codice scritto da Stiro Francesco
// L'esame in questione è stato assegnato il 24/01/17 e il 19/09/2017

#include <iostream>
#include <math.h>
#include <typeinfo>
#include <algorithm>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <iomanip>

using namespace std;

class A {
  private:
    string s;
  public:
    A(string s){
      this->s = s;
    }

    string getS(){ return this->s; }

    bool doppia(){

      for (unsigned int i=0;i<s.length();i++){
        int counter=0;
        for (unsigned int j=i+1;j<s.length();j++)
          if (s[i]==s[j])
            counter++;

        if (counter>=1)
          return true;
      }
        return false;
    }

    virtual string nuova()=0;

    virtual ostream& put(ostream& os){
			return os << "s=" << s;
		}
};

ostream & operator << (ostream& left, A& a){
	return a.put(left);
}

class B: public A{

  private:
    int y;

  public:
    B(string s, int y):A(s){
      this->y = y;
    }

    bool doppia (){
      for (unsigned int i=0;i<getS().length();i++){
        int vocali = 0;
        if (getS()[i]=='a'|| getS()[i]=='e' || getS()[i] == 'i' || getS()[i] == 'o' || getS()[i]=='u')
          for (unsigned int j=i+1; j<getS().length();j++)
            if (getS()[i] == getS()[j])
              vocali++;

        if (vocali>=1)
          return true;

      }

      return false;
    }

    string nuova(){
      stringstream ss;
      if (y%3==0){
        for (unsigned int i=getS().length()-3;i<getS().length();i++)
          ss << getS()[i];
      }
      else
          ss << getS()[0] << getS()[1] << getS()[2];

      return ss.str();

    }

    ostream & put(ostream & os){
			return A::put(os) << " y=" <<  y;
		}

};


class C: public A{

  protected:
   char c;

  public:

    C(string s, char c):A(s){
      this->c = c;
    }

    string nuova(){
      stringstream ss;
      ss << getS()[0];
      for (unsigned int i=1;i<getS().length();i++)
        ss << '*';

      return ss.str();
    }

    ostream & put(ostream & os){
			return A::put(os) << " c=" <<  c;
		}
};


class D: public C{

  public:
    D(string s, char c):C(s,c){}

    string nuova(short int x){
      stringstream ss;
      ss << c;

      for (int i=0;i<x;i++)
        ss << static_cast<char>(getS()[i]+1);

      return ss.str();
    }

    ostream & put(ostream & os){
			return C::put(os);
		}
};

int main (){

  int DIM = 30;
  A* vett[DIM];
  stringstream ss,sr;
  int counter = 0;

srand(111222333);
for (int i=0;i<DIM;i++){
  int p = rand()%5+3;
  string str = "";
  for (int j=0;j<p;j++)
    str+= (char) ('a' + rand()%26);
  if (rand()%2==1)
    vett[i] = new D(str,(char)('a'+ rand()%26));
  else
    vett[i] = new B(str,rand()%15);

  cout << *vett[i] << endl;
  ss << vett[i]->nuova()[0] << vett[i]->nuova()[1];

  if (typeid(*vett[i])==typeid(D))
    sr << ((D*)vett[i])->nuova(3)[0] << ((D*)vett[i])->nuova(3)[1] << ((D*)vett[i])->nuova(3)[2];

    if(vett[i]->doppia())
      counter++;
}

  cout << endl << "nuova = " << ss.str();
  cout << endl << "nuova(3) = " << sr.str();
  cout << endl << "doppi = " << counter;

}
