/* Autore: Luca Cavallaro
 * Consegna: Assumendo che il primo gennaio di quest'anno sia lunedi, inseriti un giorno e un mese dello stesso anno dire di che giorno della settimana si tratta
 */



#include <iostream>


using namespace std;



int main(int argc, char *argv[])
{
	
int giorno, mese, giorni_mese;

	cout << "Giorno: ";
	cin >> giorno;
	cout << "Mese: ";
	cin >> mese;

	for(mese--; mese > 0; mese--) {

		switch(mese) {
			case 2:
				giorni_mese = 28;
				break;
			
			case 4:
			case 6:
			case 9:
			case 11:
				giorni_mese = 30;
				break;
			
			default:
				giorni_mese = 31;
				break;
		}

		giorno += giorni_mese;
	}

	switch(giorno % 7) {
		case 1:
			cout << "lunedi" << endl;
			break;
		case 2:
			cout << "martedi" << endl;
			break;
		case 3:
			cout << "mercoledi" << endl;
			break;
		case 4:
			cout << "giovedi" << endl;
			break;
		case 5:
			cout << "venerdi" << endl;
			break;
		case 6:
			cout << "sabato" << endl;
			break;
		case 0:
			cout << "domenica" << endl;
			break;
	}

	return 0;
}