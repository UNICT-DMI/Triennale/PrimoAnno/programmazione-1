/////////////////////////////
////Posted By Ivan Jhahy////
///////////////////////////

////////////////////////////////////////////////////////////////////////////////////
// Testo: Codificare un programma che calcoli la seguente espressione -> y=xa+b , //
//        dove "x" � uguale a 5, "a" � uguale a 18 e "b" � uguale a 7.            //
//        Tutte le variabili devono essere di tipo int.                           //
////////////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

int main(){
           int y=0, x=5, a=18, b=7;
           y= (x*a) + b;
           cout << "Il risultato dell'operazione e': " << y << endl;
           system("PAUSE");
           return 1;
}
