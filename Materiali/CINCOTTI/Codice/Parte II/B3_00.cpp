#include <iostream>
#include "dado.h"

using namespace std;


int main()
{
    Dado d;
    long int i=0L;  
    long int vinto=0L; 
    srand(time(0));

    while (i<100000000L)
       {       
         d.effettuaLancio(); 
          
         if ( d.getUltimoLancio() == 6 )
            d.effettuaLancio();
         
         if ( d.getUltimoLancio() == 3 )
            vinto++;
         i++;
       }
    cout << "Prob. a posteriori : " << 
            (vinto/(float)i) << endl;   

    system("PAUSE");
    return EXIT_SUCCESS;
}
