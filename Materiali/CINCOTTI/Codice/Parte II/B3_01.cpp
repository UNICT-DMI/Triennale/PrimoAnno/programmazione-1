#include <iostream>
#include "dado.h"

using namespace std;


int main()
{
    srand(time(0));
    
    Dado primo, secondo;

    primo.effettuaLancio();
    secondo.effettuaLancio();

    int numeroDiLanci = 1;
     
    while (primo.getUltimoLancio() !=
           secondo.getUltimoLancio() )        
      {
        primo.effettuaLancio();
        secondo.effettuaLancio();
        numeroDiLanci++; 
      }  
      
    cout << "Sono stati effettuati " <<
      numeroDiLanci <<
      " lanci.\n" << endl;
    cout << "Primo dado   : " << primo   << endl;     
    cout << "Secondo dado : " << secondo << endl;     

    primo.commentaLancio(); 

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()
