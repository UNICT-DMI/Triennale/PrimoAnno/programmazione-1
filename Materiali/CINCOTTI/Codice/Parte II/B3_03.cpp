#include <iostream>
#include "moneta.h"

using namespace std;


int main()
{
   const int GOAL = 3;

         int cont1 = 0, 
             cont2 = 0;

   Moneta moneta1;
   Moneta moneta2;
      
   srand(time(0));
 
   while (cont1 < GOAL && cont2 < GOAL)
      {
         moneta1.effettuaLancio();
         moneta2.effettuaLancio();

         cout << "Moneta 1: " << moneta1;
         cout << "Moneta 2: " << moneta2 << endl;

         if (moneta1.getFaccia() == 'T')
             cont1++;
         else
             cont1 = 0;
         
         cont2 = (moneta2.testa() ? cont2+1 : 0);
      }

   if (cont1 < GOAL)
         cout << "Vince la moneta 2";
      else
         if (cont2 < GOAL)
            cout << "Vince la moneta 1";
         else
            cout << "Pari!";
            
   cout << endl;         

   system("PAUSE");
   return EXIT_SUCCESS;
} // end main()
