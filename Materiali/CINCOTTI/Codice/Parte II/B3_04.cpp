#include <iostream>
#include "serbatoio.h"

using namespace std;


int main()
{    
    Serbatoio s(10,7);
    cout << s;

    float inUscita = s.preleva(3);
    cout << "Prelevati: " << inUscita << " litri.\n";     
    cout << s;

    if (s.piuDiMeta())
       cout << "Abbondante!\n";
    else
       cout << "Scarseggia!\n";

    inUscita += s.svuotaTutto();
    cout << "Serbatoio svuotato!\n";    
    cout << s;

    s.deposita(8);
    cout << "Depositati: 8 litri.\n";     
    cout << s;

    cout << "Contenuto prelevato in totale "
         << "dal serbatoio : " 
         << inUscita << " litri.\n";    

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()
