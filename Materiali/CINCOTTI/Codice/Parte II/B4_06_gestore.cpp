#include <iostream>
#include "gestore_monete.h"

using namespace std;

  

int main()
{ 
   int quantitaIniziale[] = {2,3,4,5}; 
   GestoreMonete g;
   cout << g << endl;
   g.depositaMonete(quantitaIniziale);
   cout << g << endl;
    
   g.depositaUnaMoneta(5);
   g.prelevaUnaMoneta(50);
   cout << "Deposito 5,  Prelevo 50\n\n";
   cout << g << endl;

   int p = g.prelevaImporto(35);
   cout << "Richiesti 35, Prelevati " << p << endl;
   cout << g << endl;

   cout << "Richiesti 17\n";
   p = g.prelevaImporto(17);
   cout << "Prelevati " << p << endl;
   cout << g << endl;
    
   p = g.prelevaTutto();
   cout << "Richiesto tutto, Prelevati " << p << endl;
   cout << g << endl;
   
   system("PAUSE");
   return EXIT_SUCCESS;

} // End main()

