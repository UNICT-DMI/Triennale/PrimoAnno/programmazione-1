//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : C1_04.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Funzionalita' basilari
//***   Argomento      : Argomenti standard per le funzioni
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


const char Ch = 'X';


double f (float x, int j=1, char c='A');

// Piu' elegante ...
//
// double f (float, int =1, char ='A');
//

// Il seguente errore di compilazione e' dovuto al fatto
// che gli argomenti standard devono specificare tutti
// i parametri piu' a destra
//
//     double f (float x=3.14, int j, char c='A');  ... Errore
//


inline int g (int k=0)
  {
    cout << "g : " << k << '\n';
    return k;
  }


float h (int k, int m, char c=Ch);

float h (int k, int m=g(100), char c)
  {
    cout << "h : " << k << "   " << m << "   " << c << '\n';
    return (float) (k+m);
  }


double f (float x, int j, char c)
  {
    cout << "f : " << x << "   " << j << "   " << c << '\n';
    return x;
  }


int main()
  {
   f(2.3, 8, 'Z');
   f(2.3, 8);
   f(2.3, 'Z');   // Errato, anche se ... viene compilato !
   f(2.3);

   g(5);
   g();

   h(2);
   // h();  ... Errore di compilazione
   
   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
  }

//*****************************************************************
//*****************************************************************
