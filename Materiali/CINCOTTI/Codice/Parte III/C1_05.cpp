//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : C1_05.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Funzionalita' basilari
//***   Argomento      : Regole di visibilit�
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>
#include <cmath>


using namespace std;


int main()
{ 
   int  unInt = 47;
     
   // Qual e' il ciclo di vita delle seguenti variabili ? 

   {
      int a = 2,  b;  
        
      b = (int) sin(a);
         
      {
         int c = 5; 
         int d = a + c; 
         int a = 1;          // Ogni linguaggio ha le proprie regole di visibilit�:
                             // in Java tale dichiarazione genera un errore di compilazione
         cout << a << endl;  // perch� "a" e' gia' definita
      }
      
       // a += c;   // Errore di compilazione
                      // "c" non e' visibile
        
      a += unInt; 
   }
      
   for (int i=3;  i<7;  i+=2)
      {
         int inutile;   // Ciclo di vita ?
         inutile+=2;
         cout << i << " (" << inutile << ")\n";
         i--;        // Pessima idea ... 
      }
         
    // unInt = i;    // Errore di compilazione
                    // "i" non e' visibile

          
   system("PAUSE");
   return EXIT_SUCCESS;
} // End main()
