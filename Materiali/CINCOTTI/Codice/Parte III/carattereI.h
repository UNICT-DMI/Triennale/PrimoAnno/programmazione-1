#include<iostream>
#include <string>

using namespace std;

//****************
class CarattereI {
//****************
      
public:  //***************************

   void setCarattere (char x)   { c = x; }

   void visualizzaUno () { cout << c;}

   void visualizzaDieci ()
     { 
       int i=0;
       while (i<10)
          {         
             cout << c;
             i++;
          }         
     }
   
   void visualizzaN (short n)
     { 
       int i=0;
       while (i<n)
          {         
             cout << c;
             i++;
          }         
     }
   
   void visualizzaRigheDa10 (short r)
     { 
       int i=0;   
       int j;
       while (i<r)
          {      
             j=0;          
             while (j<10)
                {
                   cout << c;
                   j++;
                }
             cout << endl;
             i++;
          }  
	 }	
		
   void visualizzaRigheDaN (short r, short n)
     { 
       int i=0;   
       int j;
       while (i<r)
          {      
            j=0;          
            while (j<n)
               {
                  cout << c;
                  j++;
               }
            cout << endl;
            i++;
          }         
     }
	

private: //***************************
        
   char c;


}; // End class CarattereI

