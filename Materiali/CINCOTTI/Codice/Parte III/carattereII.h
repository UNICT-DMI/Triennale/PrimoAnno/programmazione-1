#include<iostream>
#include <string>

using namespace std;

//*****************
class CarattereII {
//*****************
      
public:  //***************************

   void setCarattere (char x)   { c = x; }

   void visualizzaUno () { cout << c;}

   void visualizzaDieci ()
     { 
       int i=0;
       while (i++<10)    visualizzaUno();
     }
   
   void visualizzaN (short n)
     { 
       int i=0;
       while (i<n)
          {         
            visualizzaUno();
            i++;
          }                 
     }
   
   void visualizzaRigheDa10 (short r)
     { 
       int i=0;   
       while (i<r)
          {  
            visualizzaDieci();          
            cout << endl;
            i++;
          }         
	 }	
		
   void visualizzaRigheDaN (short r, short n)
     { 
     int i=0;   
      while (i<r)
      {  
         visualizzaN(n);          
         cout << endl;
         i++;
      }            
     }
	

private: //***************************
        
   char c;


}; // End class CarattereII

