#include <iostream>
#include "dado.h"


Dado::Dado()
{
    effettuaLancio(); 
}


void Dado::effettuaLancio ()
   {
      valoreUltimoLancio = 
          (short) (1 + rand()%6);
   }


short int Dado::getUltimoLancio () const
   {
      return  valoreUltimoLancio;
   }
   
  
void Dado::commentaLancio () const
   {
      string Str="";
      short valoreCasuale = (short) (rand()%5);
      
      switch (valoreCasuale)
        {
          case 0 :
             Str = "E' stato un ottimo lancio!";
             break;
          case 1 :
             Str = "Il dado non si � neache mosso...";
             break;
          case 2 :
             Str = (string)"Che ti � successo? " + 
                   (string) "Ti vedo deperito...";
             break;
          case 3 :
             Str = "Potevi fare meglio!";
             break;
          case 4 :
             Str = (string) "Non avevo mai visto " +
                   "un dado acrobatico!";
             break;
        }
      cout << Str << endl << endl;  
   }
 
   
ostream &operator<< (ostream &stream, Dado d)
   {
      stream << "Il dado attualmente presenta la faccia "
             << d.getUltimoLancio() << ".\n";
      return stream;     
   }
   
