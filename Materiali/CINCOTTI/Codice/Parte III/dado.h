#ifndef DADO_H
#define DADO_H

#include <string>
using namespace std;


class Dado {
      
public:  //***************************

   Dado(); 

   short int getUltimoLancio() const;
   
   void effettuaLancio();
 
   void commentaLancio() const;  

   friend ostream &operator<< (ostream &stream, Dado d);


private: //***************************
        
   short int valoreUltimoLancio;

};   // end class Dado

#endif
