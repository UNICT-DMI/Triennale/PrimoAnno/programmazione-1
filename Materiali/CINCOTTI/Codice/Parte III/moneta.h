#ifndef MONETA_H
#define MONETA_H

#include <string>
using namespace std;


//************
class Moneta {
//************

public:  

   Moneta(); 
   
   void effettuaLancio();

   bool testa() const;
 
   bool croce() const;

   char getFaccia() const;

   friend ostream &operator<< (ostream &stream, Moneta m);
   

private: 
        
   char ultimaFaccia;

};   // end class Moneta

#endif
