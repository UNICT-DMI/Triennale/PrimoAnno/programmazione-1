#include <iostream>
#include "serbatoio.h"


Serbatoio::Serbatoio (float _capacita)
   {
      capacitaTotale   = _capacita;
      quantitaPresente = 0;
   }


Serbatoio::Serbatoio (float _capacita,
                      float _quantita)
   {
      capacitaTotale   = _capacita;
      quantitaPresente = _quantita;
   }
   
   
float Serbatoio::getCapacita () const
   {
      return  capacitaTotale;
   }
 
   
float Serbatoio::getQuantita () const
   {
      return  quantitaPresente;
   }
 
   
void Serbatoio::deposita (float _quantita)
   {
      quantitaPresente += _quantita; 
      if (quantitaPresente > capacitaTotale)
         {      
            cout << "ATTENZIONE: "
                 << "Il liquido � traboccato!\n";
            quantitaPresente = capacitaTotale;
         }
   }

   
float Serbatoio::preleva (float _quantita)
   {
      float quantitaPrelevata;
      
      if (quantitaPresente >= _quantita)
         {
           quantitaPrelevata = _quantita; 
           quantitaPresente -= _quantita;
         } 
      else
         {
           cout << "ATTENZIONE: " 
                << "La quantit� richiesta non"
                << " � presente all'interno"  
                << " del serbatoio!\n";
           quantitaPrelevata = quantitaPresente;
           quantitaPresente  = 0;
         }
         
      return  quantitaPrelevata;
   }

   
float Serbatoio::svuotaTutto ()
   {
      float quantitaPrelevata = quantitaPresente; 
      quantitaPresente = 0;
      return quantitaPrelevata;
   }

   
void Serbatoio::riempiTutto ()
   {
      quantitaPresente = capacitaTotale;
   }

   
bool Serbatoio::pieno () const
   {
      return (quantitaPresente == capacitaTotale);
   }
   
   
bool Serbatoio::vuoto () const
   {
      return (quantitaPresente < LIMITE_INFERIORE);
   }
   
   
bool Serbatoio::piuDiMeta () const
   {
      return (quantitaPresente > capacitaTotale / 2.0);
   }

      
ostream &operator<< (ostream &stream, Serbatoio s)
   {
      cout << "Il serbatoio contiene " 
           << s.getQuantita() 
           << " litri su " << s.getCapacita() 
           << " di capacit�.\nSTATO: ";
      
      if (s.quantitaPresente < s.capacitaTotale * 0.1)
         cout << "Allarme ROSSO.";
      else if (s.quantitaPresente < s.capacitaTotale * 0.25)
         cout << "Allarme Giallo.";
      else
         cout << "Normale.";
         
      cout  << endl;  
      return stream;     
   }
   

