//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC1_04.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi     
//***   Argomento      : Dichiarazione di oggetti
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Stringa
  {
    private :

       char str[80];

    public:

       void Inizializza (char * s)
	  {
	   strcpy (str, s);
	  }

       void Leggi (char * buf)
	  {
	   strcpy (buf, str);
	  }

  }; //End class Stringa


Stringa esterno;                     // Segmento dati
static Stringa statico_esterno;      // Segmento dati


int main()
  {
    char buffer[80];                 // Stack
    Stringa interno;                 // Stack

    static Stringa statico_interno;  // Segmento dati
    Stringa *ptr;                    // Stack

    ptr = new Stringa;               // Oggetto dinamico nello Heap

    delete ptr;
   
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }

//*****************************************************************
//*****************************************************************
