//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC1_05.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Accesso ai membri delle classi
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;



class Stringa
  {
    private :

       char str[80];

    public:

       void Inizializza (char * s)
	  {
	   strcpy (str, s);
	  }

       void Leggi (char * buf)
	  {
	   strcpy (buf, str);
	  }

  }; //End class Stringa


Stringa esterno;                     // Segmento dati
static Stringa statico_esterno;      // Segmento dati


int main()
  {
    char buffer[30];                 // Stack
    Stringa interno;                 // Stack

    static Stringa statico_interno;  // Segmento dati
    Stringa *ptr;                    // Stack

    ptr = new Stringa;               // Oggetto dinamico nello Heap

    statico_esterno.Inizializza ("Statico Esterno\n");
    esterno.Inizializza ("Esterno\n");
    interno.Inizializza ("Interno\n");
    statico_interno.Inizializza ("Statico Interno\n");
    ptr -> Inizializza ("Dinamico\n");

    statico_esterno.Leggi (buffer);   cout << buffer;
    esterno.Leggi (buffer);           cout << buffer;
    interno.Leggi (buffer);           cout << buffer;
    statico_interno.Leggi (buffer);   cout << buffer;
    ptr -> Leggi (buffer);            cout << buffer;

    delete ptr;

    // Puntatore ad un array di oggetti dinamici

    ptr = new Stringa[5];  // Crea un array di oggetti nello Heap

    ptr[0].Inizializza ("Primo\n");
    ptr[2].Inizializza ("Secondo\n");
    ptr[4].Inizializza ("Quinto\n");

    ptr -> Leggi (buffer);            cout << buffer;
    ptr[2].Leggi (buffer);            cout << buffer;
    (ptr+4) -> Leggi (buffer);        cout << buffer;

    delete [] ptr;
   
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }

//*****************************************************************
//*****************************************************************
