//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC1_06.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Violazione dell'encapsulation
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Classe
  {
    private :

       int dato;

    public:

       void  Set (int i)   { dato = (i>0 ? i : dato); }
       int * Get ()        { return &dato; }

  }; //End class Classe



int main()
  {
    Classe x;

    x.Set(10);
    x.Set(-5);
    cout << "Prima : " << *(x.Get()) << '\n';

    int * ptr = x.Get ();
    *ptr = -5;
    cout << "Dopo : " << *(x.Get()) << '\n';
   
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }

//*****************************************************************
//*****************************************************************
