//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC2_02.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : I puntatori a oggetti
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>
#include <string>

using namespace std;


class Stringa
  {
    private :

       char str[80];

    public:

       void Set (const char * s)    { strcpy (str, s); }
       void Get (char * buf) const  { strcpy (buf, str); }

  }; //End class Stringa



int main ()
  {
    char buffer[80];
    Stringa *ptr;

    ptr = new Stringa[3];

    ptr[0].Set ("Primo\n");
    ptr[1].Set ("Secondo\n");
    ptr[2].Set ("Terzo\n");

    ptr[0].Get (buffer);    cout << buffer;
    ptr[1].Get (buffer);    cout << buffer;
    ptr[2].Get (buffer);    cout << buffer << '\n';

    ptr -> Get (buffer);    cout << "Senza indice : " << buffer;

    delete [] ptr;

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()


//*****************************************************************
//*****************************************************************
