//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC2_06.cpp
//***   Data creazione : 05/11/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Oggetti come parametri di funzioni (I)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>
#include <string>

using namespace std;
 

const int Dim = 1000;

class Stringa
  {
    private :

       char str[Dim];

    public:

       void Set (const char * s)
	  {
	    strcpy (str, s);
	  }

       void Get (char * buf) const
	  {
	    strcpy (buf, str);
	  }

  }; //End class Stringa


void Visualizza1 (Stringa s)
  {
   char buffer[Dim];

   s.Get (buffer);
   cout << "Oggetto passato per valore : " << buffer;
  }


void Visualizza2 (Stringa & s)
  {
   char buffer[Dim];

   s.Get (buffer);
   cout << "Oggetto passato per riferimento : " << buffer;
  }


int main ()
  {
    Stringa voto;

    voto.Set ("Trenta e lode\n");
    Visualizza1 (voto);
    Visualizza2 (voto);

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()


//*****************************************************************
//*****************************************************************
