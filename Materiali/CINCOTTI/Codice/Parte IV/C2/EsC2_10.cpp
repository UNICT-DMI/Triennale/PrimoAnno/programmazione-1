//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC2_10.cpp
//***   Data creazione : 06/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Il puntatore "this"
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Classe
  {
    private :

       int numero;

    public:

       void Set (int i);
       int  Get () const;

  }; //End class Classe


void Classe::Set (int i)
  {
    this -> numero = i;
  }


int Classe::Get () const
  {
    return  this -> numero;
  }


int main ()
  {
    Classe x;

    x.Set (100);
    cout << "Output : " << x.Get() << endl;

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()




//
// OSSERVAZIONE :
// Il puntatore "this" e' necessario :
//   - per accedere a variabili locali con lo
//     stesso nome di variabili membro;
//   - nel caso di overloading di alcuni operatori.
//


// Traduzione parziale in C
// ------------------------
//
// void Classe::Set (Classe * this, int i)
//  {
//    this -> numero = i;
//  }
//
//
//int Classe::Get (Classe * this) const
//  {
//    return  this -> numero;
//  }
//
//
//void main ()
//  {
//   Classe x;
//
//    Set (&x, 100);
//    cout << "Output : " << Get (&x);
//  }
//


//*****************************************************************
//*****************************************************************
