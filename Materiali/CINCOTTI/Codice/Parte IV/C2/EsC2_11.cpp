//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC2_11.cpp
//***   Data creazione : 06/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Variabili membro "static"
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Classe
  {
    private :

       float peso;
       static int peso_totale;

    public:

       static int contatore_oggetti;

       void Inizializza (int p)
	  {
	    peso = p;
	    contatore_oggetti++;
	    peso_totale += p;
	  }

       void Visualizza (void) const
	  {
	    cout << "Peso totale : " << peso_totale
		 << "    Oggetti :" << contatore_oggetti << '\n';
	  }

  }; //End class Classe


// Le variabili statiche DEVONO essere
// inizializzate all'esterno

int Classe::peso_totale = 0; // Costituisce un'eccezione al data-hiding
int Classe::contatore_oggetti = 0;


int main ()
  {
    Classe x,y,z;

    x.Inizializza (10);
    y.Inizializza (20);
    z.Inizializza (30);

    x.Visualizza ();

    x.contatore_oggetti = 10;
    cout << "contatore : " << Classe::contatore_oggetti << '\n';

    // Classe::peso_totale = 0;        Errore di compilazione

   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()

//*****************************************************************
//*****************************************************************
