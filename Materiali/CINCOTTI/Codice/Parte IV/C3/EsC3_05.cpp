//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC3_05.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Costruttori con parametri standard
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;



class Complex
  {
    private :

       float Re;
       float Im;

    public:

       Complex (float a=0.0, float b=0.0)
	  {
	    cout << "Costruttore : (" << a << ", " << b << ")\n";
	    Re = a;
	    Im = b;
	  }

  }; //End class Complex


int main ()
  {
    Complex x;
    Complex xx();            // Errore : prototipo di funzione
    Complex y(3.3);
    Complex W(2.7, 3.14);
    Complex z = Complex(2.0);               // Poco efficiente

    // Osservazioni :
    //   1) Un costruttore non puo' essere esplicitamente richiamato
    //   2) Il costruttore di default e' quello senza parametri
    //   3) Le classi dovrebbero essere progettate con pi� costruttori

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()


//*****************************************************************
//*****************************************************************
