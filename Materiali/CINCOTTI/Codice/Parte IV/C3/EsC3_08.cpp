//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC3_08.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Inizializzazione di attributi (I)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Complex
  {
    private :

       float Re;
       float Im;

    public:

       Complex (float, float);

       void Visualizza ()
	  {
	    cout << Re << "+" << Im << "i\n";
	  }

  }; //End class Complex


Complex::Complex (float a, float b) :
	 Re(a), Im(b)
  {
    cout << "Costruttore con inizializzazione di attributi\n";
  }


int main ()
  {
    Complex c(2.4,3.7);

    c.Visualizza ();

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()



//*****************************************************************
//*****************************************************************
