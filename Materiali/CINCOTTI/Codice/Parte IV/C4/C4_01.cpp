#include <iostream>

using namespace std;

/* 
  Questa classe contiene metodi overloaded
*/

//**************
class Metodi {
//**************

public:
    // Costruttore assente 

   // questo metodo non richiede alcun parametro!
   int costante()
      { 
         return 5;
      } 

   double g (int x, double y)
      { 
         return x*y;
      } 
		   
   // questa funzione prende due double e ne fa il prodotto
   double f (double x, double y)
      { 
         return x*y;
      } 
   
   
   // se togliessi i commenti non potrei compilare
   // perch� la firma della funzione e' identica 
   // alla precedente
   
   // questo metodo prende due double e ne fa il doppio prodotto
   /*
   float f (double x, double y)
      { 
         return (float) (2*x*y);
      } 
   */
      
      
   // questo metodo prende un double e ne fa il quadrato: 
   // � una forma SOVRACCARICA di "f"
   double f (double x)
      { 
         return x*x;
      } 
   
   // questo metodo prende un double e un float 
   // e ne fa la somma: � una forma SOVRACCARICA di "f"
   double f (double x, float y)
      { 
         return x+y;
      } 
   
   // questo metodo prende un int e ne fa il doppio :
   // � una forma SOVRACCARICA di "f"      
   int f (int x)
      { 
         return 2*x;
      } 
		
}; // End class Metodi		 

		 

int main()
{
   Metodi z;	
   double d1 = 3.0;
   double d2 = 5.0;
   int    i = 7;
       
   z.g(3, 4.3f);
			
   // il compilatore decide la semantica 
   // "accoppiando" i tipi e l'ordine dei parametri
   // nei primi tre casi c'� un solo metodo possibile 
   // per fare l'accoppiamento anche con i cast automatici 

        
   cout << "Con due double : " << z.f(d1, d2) << endl;  
   cout << "Con un double : "  << z.f(d1) << endl;
   cout << "Con un int : "     << z.f(i) << endl;

   cout << "Con un float  : "  << z.f((float)5.0) << endl;
   cout << "Con int+float : "  << z.f(7, 9.0f) << endl;  

        
   // La seguente chiamata � invece ambigua: 
   // quale metodo verr� chiamato se chiamo con un double ed un int?        
//   cout << "Con un double ed un int : " << z.f(d1, i) << endl;  

    
   system("PAUSE");
   return EXIT_SUCCESS;
} // End main()
