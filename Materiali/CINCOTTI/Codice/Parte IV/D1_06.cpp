#include <iostream>

using namespace std;

/* 
   Questo programma esegue la ricerca dell'indice del 
   massimo elemento in un array
*/




int main()
{ 
   int  A[100]; 
   int  i = 0;  // indice dell'array
   int  m;      // indice del massimo

   srand(time(0));  
   while (i<100) 
      {
        A[i] = rand()%1000 + 1;
        i++;     
      }
             
   // Computazione dell'indice del massimo  
   i = 1;
   m = 0;  
   while (i<100)
      {
        if (A[m] < A[i])   m = i;
        i++; 
      }
      
   cout << "Il massimo e'" << A[m] 
        << " in posizione " << m << endl;
        
   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
