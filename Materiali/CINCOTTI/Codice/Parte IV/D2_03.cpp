#include <iostream>

using namespace std;

/* 
   Questo programma usa una funzione che 
   implementa il InsertionSort.
*/


// InsertionSort
void insertionSort (int array[], int length)
  {
    for (int index = 1; index < length; index++)
       {
         int key = array[index];
         int position = index;
   
         // shifta i valori pi� grandi di key a destra
         while (position > 0 && array[position-1] > key)
            {
              array[position] = array[position-1];
              position--;
            }
         
         array[position] = key;
       }
  }



int main()
{ 
   int numeri[] = { 8, 4, 6, 1, 2, 7, 5, 3 };

   insertionSort (numeri, 8);
   
   for (int i=0; i < 8; i++)
        cout << numeri[i] << "\t";
   cout << endl;

   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
