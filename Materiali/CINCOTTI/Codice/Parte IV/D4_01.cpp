#include <iostream>

using namespace std;

/* 
   Questo programma implementa una funzione
   ricorsiva per il calcolo del fattoriale.
*/



int  fattoriale  ( int  n )
    {
      cout << "n = " << n << endl;
       
      if  ( n == 0 )     return  1;
      
      return   n * fattoriale ( n-1 );
   }


int main()
{ 
   cout << "risultato: " << fattoriale(4) << endl;     

   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
