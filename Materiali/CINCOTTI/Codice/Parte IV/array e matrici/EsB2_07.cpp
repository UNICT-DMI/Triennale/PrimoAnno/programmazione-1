//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsB2_07.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Funzionalita' basilari
//***   Argomento      : I riferimenti (III a)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;

int & f (int  a)
  {
    return a;
  }


int main()
  { int x;
    int  i = 47;

    cout <<  "f(i) = " << &f(i) << endl;

    f(i) = 0;
    cout << "i = " <<  &i << endl;
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//  Se fosse stato ...
//
//  const int & f (int & a)
//
//  allora  f(i) = 0  avrebbe generato un errore di compilazione
//


//
// Le espressioni che restituiscono indirizzi sono
// tradotte in espressioni a puntatori
//
// int * const f (int * const pa)
//   {
//     return pa;
//   }
//
// main()
//   {
//     int  i = 47;
//
//     printf ("f(i) = %d\n", *f(&i));
//     *f(&i) = 0;
//   }
//


//*****************************************************************
//*****************************************************************
