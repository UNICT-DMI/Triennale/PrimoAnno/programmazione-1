#include <iostream>
 
using namespace std;
 
const int DIM = 7; 
 
 
// Array STATICO
 
 
void metodo(float m[], int righe)   // si pu� anche specificare la dimensione
{
  for(int i=0; i<righe; i++)
     {
       cout << m[i] << " ";      
       cout << *(m+i) << " ";            
     } 
  cout << endl;       
}


int main()
{ 
   float a[DIM];
   for(int i=0; i<DIM; i++)
      {
        a[i] = i+1;
        cout << a[i] << "  ";
      }
   cout << endl;

   metodo(a, DIM);
    
   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
