#include <iostream>
 
using namespace std;

// Array DINAMICO


float * CreaArray(int dim)
{  
   float * m = new float [dim];  // un array di "dim" elementi
   
   for (int i=0;i<dim;i++)
     {
       m[i]=i+1;
     }
   return m;
}

void EliminaArray (float * a)
{
   delete [] a;
}

void StampaArray(float *a, int dim)
{
   for (int i=0;i<dim;++i)
      {
        cout << a[i] << " ";
        cout << *(a+i) << " ";
      }
   cout<<endl;
}


int main()
{ 
   int dim=7;
   float *array;  // puntatore al tipo base

   array = CreaArray(dim);
   array[3]=0;
   StampaArray(array, dim); 
   EliminaArray(array);

   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
