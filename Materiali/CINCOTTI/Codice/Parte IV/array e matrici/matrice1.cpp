#include <iostream>
 
using namespace std;
 
const int COLONNE = 4; 
 
// Matrice STATICA

int main()
{ 
   int righe=3;
   float a[righe][COLONNE];
   for(int i=0; i<righe; i++)
      {
        for(int j=0; j<COLONNE; j++)
           {
             a[i][j]=i*COLONNE+j+1;
             cout<<a[i][j]<<"   ";
           }
        cout<<"\n";
      }
   cout << endl;


   // float * b [COLONNE];   b � un array di COLONNE puntatori a float
   // float (*b) [COLONNE];  b � un puntatore ad un array di COLONNE float
   //
   // - nel caso monodimensionale un array � un puntatore al tipo base
   // - nel caso bidimensionale una matrice � un 
   //   puntatore ad un array di COLONNE float
   //
   // Infatti la seguente dichiarazione � 
   // equivalente a quella della matrice "a":
               
   float (*b)[COLONNE];    // = new float [20][COLONNE];
                           // b[7][3] = 2.71;
   
   b=a;
   
   // tenendo presente che   a[5] = *(a+5)   si ha:
   cout << a[2][1]<< " ";     // si pu� usare la notazione con gli indici
   cout << *(a[2]+1) << " ";   // anche in forma parziale         
   cout << (*(a+2))[1] << " ";       
   cout << *(*(a+2)+1) << " "; // oppure la notazione a puntatore 

   cout << (a[0])[2*COLONNE+1] << " ";    
   cout << (*a)[2*COLONNE+1] << " ";
   cout << *((*a)+2*COLONNE+1) << " ";
   cout << *((&a[0][0])+2*COLONNE+1) << endl; 
   
   
   
   cout << b[2][1]<< " ";    
   cout << *(b[2]+1) << " ";  
   cout << (*(b+2))[1] << " ";           
   cout << *(*(b+2)+1) << " ";   
   
   cout << (b[0])[2*COLONNE+1] << " ";
   cout << (*b)[2*COLONNE+1] << " ";
   cout << *((*b)+2*COLONNE+1) << " ";
   cout << *((&b[0][0])+2*COLONNE+1) << endl << endl; 

   // N.B. : i puntatori doppi NON sono coinvolti in questa rappresentazione!
   //        Infatti :        
   cout << b << "  " << *b << "  " << &b[0][0] << endl << endl;
   //        quindi    "float b[][]"  NON � equiv. a  "float **b"
    
   // Posso prendere tutta la seconda riga?
     
   float * seconda = b[1];
   cout << "Seconda riga della matrice:\n";
   for(int j=0; j<COLONNE; j++)
      {
        cout << seconda[j] << "  ";
      }
   cout << endl;    
    
   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
