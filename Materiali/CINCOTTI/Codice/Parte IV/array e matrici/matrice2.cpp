#include <iostream>
 
using namespace std;
 
const int COLONNE = 4; 

 
 // Matrice STATICA

  
void metodo(float m[][COLONNE], int righe)
{
  for(int i=0; i<righe; i++)
     {
       for(int j=0; j<COLONNE; j++)
          {
            cout << m[i][j]<< " ";      
            cout << *(m[i]+j) << " ";            
            cout << *(*(m+i)+j) << " ";
            cout << (*m)[i*COLONNE+j] << " ";            
            cout << *(*(m)+i*COLONNE+j) << "   "; 
           
          } 
       cout << endl;       
     }           
  cout <<  endl;
}


int main()
{ 
   int righe=3;
   float a[righe][COLONNE];
   for(int i=0; i<righe; i++)
      {
        for(int j=0; j<COLONNE; j++)
           {
             a[i][j]=i*COLONNE+j+1;
             cout<<a[i][j]<<"   ";
           }
        cout<<"\n";
      }
   cout << endl;

   metodo(a, righe);
    
   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
