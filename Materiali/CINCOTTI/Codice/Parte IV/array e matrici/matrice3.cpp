#include <iostream>
 
using namespace std;
 

// Matrice STATICA

 
void metodo(float *m, int righe, int colonne)
{
  for(int i=0; i<righe; i++)
     {
       for(int j=0; j<colonne; j++)
          {                
            cout << m[i*colonne+j] << " ";   // concettualmente m[i][j]
            cout << *(m+i*colonne+j) << "   "; 
           
          } 
       cout << endl;       
     }           
  cout <<  endl;
}


int main()
{ 
   int righe=3;
   int colonne = 4;
   float a[righe][colonne];
   for(int i=0; i<righe; i++)
      {
        for(int j=0; j<colonne; j++)
           {
             a[i][j]=i*colonne+j+1;
             cout<<a[i][j]<<"   ";
           }
        cout<<"\n";
      }
   cout << endl;

   metodo(*a, righe, colonne);       // si provi a togliere '*' !
   metodo(&(a[0][0]), righe, colonne);
    
   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
