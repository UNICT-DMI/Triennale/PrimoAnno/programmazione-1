#include <iostream>
 
using namespace std;
 



int main () {
   double  a=2, b=5;
   void *p;
   double *d = &b;
   
   p = d;
   // d = p; // Errore di compilazione!
   cout << *(double*)p << endl;
   
   p = &a;
   // b = *p;       Errore di compilazione!
   b = * (double *)  p;
   cout << a << " " << b << endl;
       
   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
