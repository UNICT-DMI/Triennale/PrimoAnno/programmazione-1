//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsD1_04.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Overload di operatori
//***   Argomento      : Operatori prefissi e postfissi
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Vettore3D
  {
    private :

       float    x, y, z;

    public:

       Vettore3D (float l=0.0, float m=0.0, float n=0.0) :
	      x(l), y(m), z(n)  
          { cout << " COS ";}

       void Visualizza (void) const
	   { cout << "( " << x << ", " << y << ", " << z << " )"; }

       Vettore3D operator ++ ();                          // prefisso
       Vettore3D operator ++ (int);                       // postfisso

       friend Vettore3D & operator -- (Vettore3D &);        // prefisso
       friend Vettore3D operator -- (Vettore3D &, int);   // postfisso

  }; //End class Vettore3D


// Operatore unario prefisso implementato come membro
Vettore3D Vettore3D::operator ++ ()
  {
    x++;
    y++;
    z++;
    return  *this;
  }


// Operatore unario postfisso implementato come membro
Vettore3D Vettore3D::operator ++ (int)
  {
    Vettore3D aux = *this;
    x++;
    y++;
    z++;
    return  aux;
  }


// Operatore unario prefisso implementato come "friend"
Vettore3D & operator -- (Vettore3D & operando)
  {
    operando.x--;
    operando.y--;
    operando.z--;
    return  operando;
  }


// Operatore unario postfisso implementato come "friend"
Vettore3D operator -- (Vettore3D & operando, int)
  {
    Vettore3D aux = operando;
    operando.x--;
    operando.y--;
    operando.z--;
    return  aux;
  }


int main ()
  {
    Vettore3D a (1,1,1);
    Vettore3D b, c;

    cout << "\n\n";

    a.Visualizza();    b.Visualizza();
    cout << "\n\n";
    b = --a;
    a.Visualizza();    b.Visualizza();
    cout << "\n\n";
    b = a--;
    a.Visualizza();    b.Visualizza();
    cout << "\n\n\n";

    cout << "Questo non e' corretto ...\n";
    b = (++a)++;
    a.Visualizza();    b.Visualizza();
    cout << "\n\n";

    cout << "Questo invece e' corretto ...\n";
    b = (--a)--;
    a.Visualizza();    b.Visualizza();
    cout << "\n\n";
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
