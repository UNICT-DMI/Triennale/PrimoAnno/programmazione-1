//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsD1_05.cpp
//***   Data creazione : 06/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Overload di operatori
//***   Argomento      : L'operatore "="
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>


using namespace std;


class Messaggio
  {
    private :

       char * testo;
       char * mittente;
       int    priorita;

       void Set (const char *, const char *, int);

    public:

       Messaggio (const char *, const char * = "", int = 0);
       Messaggio (const Messaggio &);   // Costruttore di copia
       ~Messaggio ();

       const Messaggio &  operator = (const Messaggio &);
  
       void Visualizza (void) const;

  }; //End class Messaggio


void Messaggio::Set (const char * t, const char * m, int p)
  {
    testo = new char [strlen(t)+1];
    mittente = new char [strlen(m)+1];
    strcpy (testo, t);
    strcpy (mittente, m);
    priorita = p;
  }


Messaggio::Messaggio (const Messaggio & mess)
  {
    Set (mess.testo, mess.mittente, mess.priorita);
    cout << "Costruttore di copia\n";
  }


Messaggio::Messaggio (const char * t, const char * m, int p)
  {
    Set (t,m,p);
  }


Messaggio::~Messaggio ()
  {
    cout << "DIS" << endl;                  
    delete [] testo;
    delete [] mittente;
  }


// - La & � presente solo per ragioni di efficienza
// - il primo const � presente per evitare modifiche 
//   all'operando di sinistra nel main, il motivo si evince nel main
// - il secondo const � presente per evitare modifiche 
//   all'operando di destra da parte del metodo sottostante
const Messaggio & Messaggio::operator = (const Messaggio & mess)
  {
    cout << "Overload = \n";
    if (this != & mess)
       {
	     this->~Messaggio ();   // Perch� qui � necessaria la "delete" delle variabili? 
	     Set (mess.testo, mess.mittente, mess.priorita);
       }   
    return *this;
  }


void Messaggio::Visualizza (void) const
  {
    cout << "Mitt.: " << mittente << "  Mess.: " << testo
	 << "  Liv. priorit� : " << priorita << '\n';
  }


int main ()
  {
    Messaggio a("Primo", "abc", 3);
    Messaggio b = a;
    Messaggio c("Secondo", "def", 1);

    cout << "\n";

    a = b = c;
    
   // (b=a) = c; 
   // Errore : l'uso di "const" impedisce la compilazione;
   // l'oggetto (b=a) � "const" in quanto alias "const &" e pertanto 
   // pu� richiamare soltanto metodi "const", � lo stesso di 
   // const Messaggio &y = a;
   // y.metodo();   compila solo se metodo() � pubblico e "const".
 
   // Se togliessi il "const" avrei a="Primo" e b=c="Secondo"
    
    a.Visualizza();
    b.Visualizza();
    c.Visualizza();
    cout << "\n\n";
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
