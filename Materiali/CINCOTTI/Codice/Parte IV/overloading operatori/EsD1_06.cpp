//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsD1_06.cpp
//***   Data creazione : 06/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Overload di operatori
//***   Argomento      : Operatore "[]"
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;

const int Dimensione = 10;

class Array
  {
    private :

       float  A[Dimensione];

    public:

       float &  operator [] (const int);

  }; //End class Array


float &  Array::operator [] (const int indice)
  {
    if (indice < 0 || indice > Dimensione-1)
      {
	    cout << "Modulo\n";
      }
    return A[indice % Dimensione];
  }



int main ()
  {
    Array a;
    int i=3;

    a[i++]  = 3.14;  // In quanto l'operatore restituisce un indirizzo
    a[--i+1] = a[3];
    cout << a[3] << "   " << a[13] << "\n\n";

    a[23] = 0.0;
    cout << a[3] << "   " << a[13] << '\n';
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
    
  }


//*****************************************************************
//*****************************************************************
