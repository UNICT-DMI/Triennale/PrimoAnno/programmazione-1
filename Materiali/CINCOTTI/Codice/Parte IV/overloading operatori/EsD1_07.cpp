//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : Matrix.cpp
//***   Data creazione : 21/01/2014
//***   Progetto       : OOP in "C++"
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;

const int rows = 2,   cols = 3;


class Matrix
  {
    private :

      float element [rows] [cols];

    public :
      Matrix (float value = 0.0);

      float    operator () (int i, int j);
      Matrix & operator =  (const Matrix & m);
      Matrix   operator +  () const;
      Matrix   operator +  (const Matrix & m) const;
      Matrix   operator -  () const;
      Matrix   operator -  (const Matrix & m) const;
      Matrix   operator *  (const Matrix & m) const;
      Matrix   operator += (const float & x);

      void Show () const;

  }; // End class Matrix


Matrix::Matrix (float value)
  {
    for (int i=0; i<rows; i++)
	for (int j=0; j<cols; j++)
	     element [i] [j] = value;
  }


float Matrix::operator () (int i, int j)
  {
    cout << "Indicizzazione overloaded\n";

    return  element [i] [j];
  }


Matrix  Matrix::operator + (const Matrix & m) const
  {
    Matrix result;

    for (int i=0; i<rows; i++)
	 for (int j=0; j<cols; j++)
	      result.element[i][j] = element[i][j] + m.element[i][j];

    return result;
  }


void Matrix::Show () const
  {
    for (int i=0; i<rows; i++)
       {
	 for (int j=0; j<cols; j++)
	       cout << element[i][j] << '\t';
	 cout << '\n';
       }
  }



int main ()
  {
    Matrix A(1);
    cout << "A = \n";    A.Show();

    Matrix B = A+A+A;
    cout << "B = \n";    B.Show();

    cout << "B(0,0) = " << B(0,0) << "    B(1,2) = " << B(1,2) << endl;

    // Cosa bisogna modificare per poter scrivere :
    //
    //                B(1,2)=9;      // Errore di compilazione ...

    cout << endl;
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************

