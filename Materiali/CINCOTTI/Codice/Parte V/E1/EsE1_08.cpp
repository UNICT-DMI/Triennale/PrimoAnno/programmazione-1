//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsE1_08.cpp
//***   Data creazione : 12/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Ereditarieta'
//***   Argomento      : Ereditarieta' multipla
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;

class Base1
  {
    protected:

       int x1;

    public:

       Base1 () : x1(0)  { cout << "Costruttore 'Base1'\n"; }
       Base1 (int i) : x1(i)  { }

       ~Base1 ()  { cout << "Distruttore 'Base1'\n"; }

  }; //End class Base1



class Base2
  {
    protected:

       int x2;

    public:

       Base2 () : x2(0)  { cout << "Costruttore 'Base2'\n"; }
       Base2 (int i) : x2(i)  { }

       ~Base2 ()  { cout << "Distruttore 'Base2'\n"; }

  }; //End class Base2



class Derivata1 : public Base1
  {
    protected:

       int y1;

    public:

       Derivata1 () : y1(0)  { cout << "Costruttore 'Derivata1'\n"; }
       Derivata1 (int i, int j) : Base1(i), y1(j)  { }

       ~Derivata1 ()  { cout << "Distruttore 'Derivata1'\n"; }


  }; //End class Derivata1



class Derivata2 : public Base2
  {
    protected:

       int y2;

    public:

       Derivata2 () : y2(0)  { cout << "Costruttore 'Derivata2'\n"; }
       Derivata2 (int i, int j) : Base2(i), y2(j)  { }

       ~Derivata2 ()  { cout << "Distruttore 'Derivata2'\n"; }


  }; //End class Derivata2



class Derivata : public Derivata1, public Derivata2
  {
    protected:

       int z;

    public:

       Derivata () : z(0)  { cout << "Costruttore 'Derivata'\n"; }
       Derivata (int i1, int j1, int i2, int j2, int k) :
		Derivata1 (i1,j1), Derivata2(i2,j2), z(k)   { }

       ~Derivata ()  { cout << "Distruttore 'Derivata'\n"; }

       void Visualizza () const
	  {
	    cout << x1 << "  " << y1 << "  "
		 << x2 << "  " << y2 << "  " << z << '\n';
	  }

  }; //End class Derivata



int main ()
  {
    Derivata a, b(1,2,3,4,5);

    a.Visualizza ();
    b.Visualizza ();
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
