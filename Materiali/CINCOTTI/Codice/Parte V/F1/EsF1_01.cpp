//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsF1_01.cpp
//***   Data creazione : 15/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Polimorfismo
//***   Argomento      : Puntatori a classi derivate (I)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;

const int Dimensione = 20;


class Base
  {
    private :

       char  nome[Dimensione+1];

    public:

       void Set_Nome (char *str)        { strcpy (nome, str); }
       void Visualizza_Nome () const    { cout << nome << '\n'; }

  }; //End class Base


class Derivata : public Base
  {
    private :

       int numero;

    public:

       void Set_Numero (int i)          { numero = i; }
       void Visualizza_Numero () const  { cout << numero << '\n'; }

  }; //End class Derivata



int main ()
  {
    Base b, *ptr_b;
    Derivata d;

    ptr_b = & b;
    ptr_b -> Set_Nome ("Base");

    ptr_b = & d;                   // Importante !!!
    ptr_b -> Set_Nome ("Derivata");
    // ptr_b -> Set_Numero (33);   // Errore di compilazione
    b.Visualizza_Nome ();
    d.Visualizza_Nome ();


    Derivata *ptr_d;

    // ptr_d = & b;    // Errore di compilazione
    ptr_d = & d;
    ptr_d -> Set_Numero (33);
    ptr_d -> Visualizza_Nome ();   // Equivalente a ptr_b
    ptr_d -> Visualizza_Numero ();

    // Tuttavia mediante una conversione di tipo ...
    ((Derivata *) ptr_b) -> Set_Numero (22);
    d.Visualizza_Numero ();
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
