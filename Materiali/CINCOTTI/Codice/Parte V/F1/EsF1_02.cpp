//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsF1_02.cpp
//***   Data creazione : 15/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Polimorfismo
//***   Argomento      : Puntatori a classi derivate (II)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Base
  {
    public:

       void f ()   { cout << "Funzione 'f' in Base\n"; }

  }; //End class Base


class Derivata1 : public Base
  {
    public:

       void f ()   { cout << "Funzione 'f' in Derivata1\n"; }
       void g ()   { return; }

  }; //End class Derivata1


class Derivata2 : public Derivata1
  {
    public:

       void f ()   { cout << "Funzione 'f' in Derivata2\n"; }

  }; //End class Derivata2



int main ()
  {
    Base b, *ptr_b;
    Derivata1 uno;
    Derivata2 due;

    ptr_b = & b;
    ptr_b -> f ();

    cout << "Chiamata mediante oggetto   : ";
    uno.f ();
    ptr_b = & uno;
    cout << "Chiamata mediante puntatore : ";
    ptr_b -> f ();
    // ptr_b -> g ();   // Errore di compilazione ...

    ptr_b = & due;
    ptr_b -> f ();
 
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
