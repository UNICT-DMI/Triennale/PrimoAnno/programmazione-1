//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsF1_05.cpp
//***   Data creazione : 15/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Polimorfismo
//***   Argomento      : Funzioni virtuali (III)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;




class Base
  {
    public:

       virtual void f ()   { cout << "Base\n"; }

  }; //End class Base


class Derivata1 : public Base
  {
    public:

       void f ()   { cout << "Prima derivazione\n"; }

  }; //End class Derivata1


class Derivata2 : public Base
  {
    public:
       // La f() non e' definita ...

  }; //End class Derivata2



int main ()
  {
    Base b, *ptr_b;
    Derivata1 uno;
    Derivata2 due;

    ptr_b = & b;
    ptr_b -> f ();


    ptr_b = & uno;
    ptr_b -> f ();

    ptr_b = & due;
    ptr_b -> f ();
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
