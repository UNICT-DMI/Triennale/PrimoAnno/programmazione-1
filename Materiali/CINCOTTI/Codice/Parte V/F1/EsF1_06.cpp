//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsF1_06.cpp
//***   Data creazione : 15/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Polimorfismo
//***   Argomento      : Funzioni virtuali (IV)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;



class Base
  {
    public:

       virtual void Uno ()   { cout << "Uno in Base\n"; }
       virtual void Due ()   { cout << "Due in Base\n"; }
       virtual void Tre ()   { cout << "Tre in Base\n"; }

  }; //End class Base


class Derivata1 : public Base
  {
    public:

       void Uno ()    { cout << "Uno in Derivata1\n"; }
       void Due ()    { cout << "Due in Derivata1\n"; }

  }; //End class Derivata1


class Derivata2 : public Derivata1
  {
    public:

       void Uno ()        { cout << "Uno in Derivata2\n"; }
       void Quattro ()    { cout << "Quattro in Derivata2\n"; }

  }; //End class Derivata2



void f (Base * ptr)
  {
    ptr -> Uno ();
    ptr -> Due ();
    ptr -> Tre ();
    cout << endl;
  };


int main ()
  {
    Base a;
    Derivata1 b;
    Derivata2 c;

    a.Uno();
    b.Uno();
    c.Uno();
    cout << endl;

    f (& a);
    f (& b);
    f (& c);


    Derivata1 * ptr_d1;

    // ptr_d1 = & a;   // Errore di compilazione
    ptr_d1 = & c;
    ptr_d1 -> Uno();

    // ptr_d1 -> Quattro();   // Errore di compilazione
    // Tuttavia si puo' utilizzare una conversione di tipo ...
    ((Derivata2*) ptr_d1) -> Quattro();
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
