//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsF1_07.cpp
//***   Data creazione : 15/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Polimorfismo
//***   Argomento      : Polimorfismo a tempo di esecuzione (I)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;



class Figura
  {
    protected:

       double  x, y;

    public:

       void Set (float a, float b)   { x = a;  y = b; }

       virtual void Area ()
	  { cout << "Non definita per questa classe\n"; }

  }; //End class Figura


class Triangolo : public Figura
  {
    public:

       void Area ()
	  {
	    cout << "Triangolo  : base = " << x << " altezza = " << y << '\n';
	    cout << "             Area = " << (x*y)/2 << "\n\n";
	  }

  }; //End class Triangolo


class Rettangolo : public Figura
  {
    public:

       void Area ()
	  {
	    cout << "Rettangolo : base = " << x << " altezza = " << y << '\n';
	    cout << "             Area = " << x*y << "\n\n";
	  }

  }; //End class Rettangolo



int main ()
  {
    Figura     *ptr;
    Triangolo  t;
    Rettangolo r;

    ptr = & t;
    ptr -> Set (3.7, 9.2);
    ptr -> Area ();

    ptr = & r;
    ptr -> Set (3.7, 9.2);
    ptr -> Area ();
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
