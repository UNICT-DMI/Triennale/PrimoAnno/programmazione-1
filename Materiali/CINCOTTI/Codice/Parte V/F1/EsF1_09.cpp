//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsF1_09.cpp
//***   Data creazione : 15/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Polimorfismo
//***   Argomento      : Classi astratte
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;



class Figura
  {
    protected:

       double  x, y;

    public:

       void Set (float a, float b = 0.0)   { x = a;  y = b; }

       virtual void Area () = 0;       // Funzione virtuale pura

  }; //End class Figura


class Triangolo : public Figura
  {
    public:

       void Area ()
	  {
	    cout << "Triangolo  : base = " << x << " altezza = " << y << '\n';
	    cout << "             Area = " << (x*y)/2 << "\n\n";
	  }

  }; //End class Triangolo


class Rettangolo : public Figura
  {
    public:

       void Area ()
	  {
	    cout << "Rettangolo : base = " << x << " altezza = " << y << '\n';
	    cout << "             Area = " << x*y << "\n\n";
	  }

  }; //End class Rettangolo


class Cerchio : public Figura
  {
    public:

       // La seguente funzione DEVE necessariamente essere definita

       void Area ()
	  {
	    cout << "Cerchio    : raggio = " << x << '\n';
	    cout << "             Area = " << 3.14 *x*x << "\n\n";
	  }

  }; //End class Cerchio



int main ()
  {
    Figura     *ptr;
    // Figura  f;    // Non si possono definire oggetti di classi astratte
    Triangolo  t;
    Rettangolo r;
    Cerchio    c;


    ptr = & t;
    ptr -> Set (3.7, 9.2);
    ptr -> Area ();

    ptr = & r;
    ptr -> Set (3.7, 9.2);
    ptr -> Area ();

    ptr = & c;
    ptr -> Set (1.0);
    ptr -> Area ();
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
