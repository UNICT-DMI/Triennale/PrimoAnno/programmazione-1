#include <iostream>

using namespace std;

#include <iostream>
using namespace std;

class base {
public:
  virtual void vfunc() {
    cout << "This is base's vfunc().\n";
  }
  void print() {vfunc();}
};

class derived1 : public base {
public:
  void vfunc() {
    cout << "This is derived1's vfunc().\n";
  }
};

class derived2 : public derived1 {
public:
  void vfunc() {
    cout << "This is derived2's vfunc().\n";
  }
};

void help (base & b)    // senza "&" cosa accade ???
   {
      b.vfunc();
   }
           

int main()
{
    
    cout << int('\0') << endl;
     const int c = 56;
     const int * ptr = &c;
     //*ptr=50;
     cout << c << endl;
  base *p, b;
  derived1 d1;
  derived2 d2;

  d2.vfunc(); cout <<"\n\n\n"; 

   b.print(); 
  d1.print();  
  d2.print(); cout <<"\n\n\n"; 

  help(b);   
  help(d1);  
  help(d2); cout <<"\n\n\n"; 


  // point to base
  p = &b;
  p->vfunc(); // access base's vfunc()

  // point to derived1
  p = &d1;
  p->vfunc(); // access derived1's vfunc()

  // point to derived2
  p = &d2;
  p->vfunc(); // access derived2's vfunc()

    
   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
