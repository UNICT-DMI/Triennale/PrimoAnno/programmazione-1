//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsG1_01.cpp
//***   Data creazione : 17/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Template
//***   Argomento      : Funzioni generiche (I)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


template <class T>
void Scambia (T & a,  T & b)
  {
    T aux;

    aux = a;
    a = b;
    b = aux;
  }


// Almeno un parametro della funzione generica deve
// essere del tipo specificato nel template ...

template <class T>   T Somma (T,  int);

// Il seguente prototipo genera un errore di compilazione ...
//
//     template <class T>   T Operazione (float,  float);


int main ()
  {
    int   i = 6,    j = 9;
    float x = 1.6,  y = 3.3;
    char  a = 'a',  b = 'b';

    cout << "Prima : " << i << "  " << j << "\n";
    Scambia (i, j);
    cout << "Dopo  : " << i << "  " << j << "\n\n";

    cout << "Prima : " << x << "  " << y << "\n";
    Scambia (x, y);
    cout << "Dopo  : " << x << "  " << y << "\n\n";

    cout << "Prima : " << a << "  " << b << "\n";
    Scambia (a, b);
    cout << "Dopo  : " << a << "  " << b << "\n\n";

    // Scambia (i, x);   // Errore di compilazione ...
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
