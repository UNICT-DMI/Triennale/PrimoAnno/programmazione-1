//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsG1_02.cpp
//***   Data creazione : 17/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Template
//***   Argomento      : Funzioni generiche (II)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


template <class T>
T Minimo (T a,  T b)
  {
    cout << "Funzione generica : ";
    return  (a < b ? a : b);
  }


double Minimo (double a,  double b)
  {
    cout << "Funzione normale  : ";
    return  (a < b ? a : b);
  }

//
// Le famiglie di funzioni di overloading possono
// contenere funzioni generiche, in questo caso ...
//
// Regole di corrispondenza degli argomenti :
//
//   1) Si cerca una corrispondenza esatta con qualche funzione normale;
//   2) Si cerca una corrispondenza esatta con qualche funzione generica;
//   3) si applicano le regole standard di risoluzione
//      dell'overloading solo alle funzioni normali.
//

int main ()
  {
    int    i = 6,    j = 9;
    double x = 1.6,  y = 3.6;

    cout << Minimo (i, j) << endl;    // passo 2)
    cout << Minimo (x, y) << endl;    // passo 1)
    cout << Minimo (i, x) << endl;    // passo 3)
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
