//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsG1_03.cpp
//***   Data creazione : 17/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Template
//***   Argomento      : Funzioni generiche (III)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;

//
// Si possono specificare piu' tipi generici
//

template <class T1, class T2>
double minimo (T1 a, T2 b)
  {
    return  (a < b ? a : b);
  }


// Il seguente prototipo genera un errore di compilazione ...
//
//template <class T1, class T2>   T1 & Operazione (float, T2 b)


int main ()
  {
    int     i = 6,     j = 9;
    float   x = 1.6,   y = 3.3;
    double  d = 5.5;

    cout << minimo (i, j) << endl;   // T1 = T2 = int
    cout << minimo (x, y) << endl;   // T1 = T2 = float
    cout << minimo (d, i) << endl;   // T1 = double,  T2 = int
    cout << minimo (x, d) << endl;   // T1 = float,   T2 = double

    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
