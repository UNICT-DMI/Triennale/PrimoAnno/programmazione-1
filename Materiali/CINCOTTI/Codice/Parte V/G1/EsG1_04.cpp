//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsG1_04.cpp
//***   Data creazione : 17/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Template
//***   Argomento      : Classi generiche (I)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


template <class T>
class Stack
  {
    private :

      int dimensione;     // Capacita' dello stack
      int top;            // Punta alla prima locazione libera
      T * ptr;            // Punta ai dati contenuti nello heap

    public :
      Stack (int);
      ~Stack ();
      void Push (T);
      T Pop ();
  };



template <class T>
Stack<T>::Stack (int i)
  {
    dimensione = i;
    top = 0;
    ptr = new T [dimensione];
  }


template <class T>
Stack<T>::~Stack ()
  {
    delete [] ptr;
  }

template <class T>
void Stack<T>::Push (T dato)
  {
    if (top < dimensione)    ptr [top++] = dato;
  }


template <class T>
T Stack<T>::Pop ()
  {
    if (!top)  return 0;

    return  ptr [--top];
  }


template <class T>
void Svuota (Stack<T> & s)
  {
    T dato;
    while (dato = s.Pop())
       cout << dato << '\n';
    cout << endl;
  }



int main ()
  {
    Stack <int>   Interi (10);
    Stack <float> Reali (10);

    Interi.Push (3);
    Interi.Push (4);
    Interi.Push (5);
    Svuota (Interi);

    Reali.Push (1.1);
    Reali.Push (2.2);
    Reali.Push (3.3);
    Svuota (Reali);
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
