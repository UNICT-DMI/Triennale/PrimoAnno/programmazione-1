//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsG1_05.cpp
//***   Data creazione : 17/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Template
//***   Argomento      : Classi generiche (II)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;

//
// I parametri dei template possono essere di uno dei tipi
// di dati fondamentali e i corrispondenti argomenti devono
// essere espressioni costanti.
//

// Per l'uso del template, "typename" � equivalente a "class"

template <typename T, int DIM>
class Stack
  {
    private :

      int top;            // Punta alla prima locazione libera
      T   dati [DIM];     // Contiene i dati

    public :

      Stack () : top(0)  { }
      ~Stack ()  { };
      void Push (T);
      T Pop ();
  };


template <typename T, int DIM>
void Stack<T, DIM>::Push (T dato)
  {
    if (top < DIM)    dati [top++] = dato;
  }


template <typename T, int DIM>
T Stack<T, DIM>::Pop ()
  {
    if (!top)  return 0;

    return  dati [--top];
  }



int main ()
  {
    Stack <float, 10> Reali;

    Reali.Push (1.1);
    Reali.Push (2.2);
    Reali.Push (3.3);

    cout << Reali.Pop () << '\t';
    cout << Reali.Pop () << '\t';
    cout << Reali.Pop () << '\t';
    cout << Reali.Pop () << '\n';
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
