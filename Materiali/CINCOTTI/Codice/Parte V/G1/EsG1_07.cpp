//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsG1_07.cpp
//***   Data creazione : 17/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Template
//***   Argomento      : Classi generiche (I)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Complex
  {
    private :

       float Re;
       float Im;

    public:

       Complex ( );
       Complex (float); 
       Complex (float, float);

       void Visualizza (void) const;
       
       friend ostream &  operator << (ostream & sx, const Complex & dx);

  }; //End class Complex


Complex::Complex ( ) :
	 Re(0), Im(0)
  {
    cout << "Costruttore di default\n";
  }


Complex::Complex ( float a) :
	 Re(a), Im(0)
  {
    cout << "Costruttore con un parametro\n";
  }


Complex::Complex (float a, float b) :
	 Re(a), Im(b)
  {
    cout << "Costruttore Complex\n";
  }


void Complex::Visualizza (void) const
  {
    cout << Re << "+" << Im << "i\n";
  }

ostream &  operator << (ostream & sx, const Complex & dx)
  {
    return sx << dx.Re << "+" << dx.Im << "i";
  }



// ********************************************************
// ********************************************************



template <class T>
class Stack
  {
    private :

      int dimensione;     // Capacita' dello stack
      int top;            // Punta alla prima locazione libera
      T * ptr;            // Punta ai dati contenuti nello heap

    public :
      Stack (int);
      ~Stack ();
      void Push (T);
      T Pop ();
      bool empty ();
  };



template <class T>
Stack<T>::Stack (int i)
  {
    dimensione = i;
    top = 0;
    ptr = new T [dimensione];
  }


template <class T>
Stack<T>::~Stack ()
  {
    delete [] ptr;
  }

template <class T>
void Stack<T>::Push (T dato)
  {
    if (top < dimensione)    ptr [top++] = dato;
  }

template <class T>
T Stack<T>::Pop ()
  {
    if (!top)  return 0;   // La soluzione professionale fa uso delle eccezioni 

    return  ptr [--top];
  }

template <class T>
bool Stack<T>::empty ()
  {
    return (top == 0);
  }



template <class T>
void Svuota (Stack<T> & s)
  {
    T dato;
    while (!s.empty())  
       { 
         dato = s.Pop();
         cout << dato << '\n';     
       }   
    cout << endl;
  }



int main ()
  {         
    Stack <Complex>  s (10);

    s.Push (Complex(3,3));
    s.Push (Complex(-4,-4));
    s.Push (Complex(5,5));
    
    Svuota (s);

    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
