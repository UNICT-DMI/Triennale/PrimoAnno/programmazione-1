//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsG1_09.cpp
//***   Data creazione : 17/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Template
//***   Argomento      : Classi generiche (I)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;

template <class T> class Complex;

template <class T>
ostream &  operator << (ostream & sx, const Complex<T> & dx);

// le due dichiarazioni forward sono necessarie per evitare un problema al linker




template <class T>
class Complex
  {
    private :  

       T Re;
       T Im;

    public:

       Complex ( );
       Complex (T); 
       Complex (T, T);

       void Visualizza (void) const;

       // Le <> si inseriscono per evitare il problema al linker
       friend ostream &  operator << <> (ostream & sx, const Complex<T> & dx);

  }; //End class Complex


template <class T>
Complex<T>::Complex ( ) :
	 Re(0), Im(0)
  {
    cout << "Costruttore di default\n";
  }


template <class T>
Complex<T>::Complex ( T a) :
	 Re(a), Im(0)
  {
    cout << "Costruttore con un parametro\n";
  }


template <class T>
Complex<T>::Complex (T a, T b) :
	 Re(a), Im(b)
  {
    cout << "Costruttore Complex\n";
  }


template <class T>
void Complex<T>::Visualizza (void) const
  {
    cout << Re << "+" << Im << "i\n";
  }

template <class T>
ostream &  operator << (ostream & sx, const Complex<T> & dx)
  {
    return sx << dx.Re << "+" << dx.Im << "i";
  }



// ********************************************************
// ********************************************************


template <template <class> class T, class S>
class Stack
  {
    private :

      int dimensione;     // Capacita' dello stack
      int top;            // Punta alla prima locazione libera
      T<S> * ptr;            // Punta ai dati contenuti nello heap
      
      S foo;              // per accedere al sottotipo S, altrimenti 
                          // impossibile nella soluzione precedente

    public :
      Stack (int);
      ~Stack ();
      void Push (T<S>);
      T<S> Pop ();
      bool empty ();
  };



template <template <class> class T, class S>
Stack<T,S>::Stack (int i)
  {
    dimensione = i;
    top = 0;
    ptr = new T<S> [dimensione];
  }


template <template <class> class T, class S>
Stack<T,S>::~Stack ()
  {
    delete [] ptr;
  }

template <template <class> class T, class S>
void Stack<T,S>::Push (T<S> dato)
  {
    if (top < dimensione)    ptr [top++] = dato;
  }

template <template <class> class T, class S>
T<S> Stack<T,S>::Pop ()
  {
    if (!top)  return 0;   // La soluzione professionale fa uso delle eccezioni 

    return  ptr [--top];
  }

template <template <class> class T, class S>
bool Stack<T,S>::empty ()
  {
    return (top == 0);
  }



template <template <class> class T, class S>
void Svuota (Stack<T,S> & s)
  {
    T<S> dato;
    while (!s.empty())  
       { 
         dato = s.Pop();
         cout << dato << '\n';     
       }   
    cout << endl;
  }



int main ()
  {             
    Stack <Complex,double>  s (10);

    s.Push (Complex<double>(3.3,3.3));
    s.Push (Complex<double>(-4.4,-4.4));
    s.Push (Complex<double>(5.5,5.5));
    
    Svuota (s);

    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
