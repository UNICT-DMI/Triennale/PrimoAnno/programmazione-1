// An example that uses typeid on a polymorphic class heirarchy.
#include <iostream>
#include <typeinfo>
using namespace std;

class Mammal {
public:
  //virtual 
  bool lays_eggs() { return false; } // Mammal is polymorphic
  // ... provate ad eliminare "virtual"
};

class Cat: public Mammal {
public:
  // ...
};

class Platypus: public Mammal {
public:
  bool lays_eggs() { return true; }
  // ...
};


void WhatMammal(Mammal &ob)
{
  cout << "\nob is referencing an object of type ";
  cout << typeid(ob).name() << endl;
  if(typeid(ob) == typeid(Cat)) 
    cout << "Cats don't like water.\n\n";
}



int main()
{
  Mammal *p, AnyMammal;
  Cat cat;
  Platypus platypus;

  p = &AnyMammal;
  cout << "p is pointing to an object of type ";
  cout << typeid(*p).name() << endl;

  p = &cat;
  cout << "p is pointing to an object of type ";
  cout << typeid(*p).name() << endl;

  p = &platypus;
  cout << "p is pointing to an object of type ";
  cout << typeid(*p).name() << endl << endl;

  WhatMammal(AnyMammal);
  WhatMammal(cat);
  WhatMammal(platypus);

   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
