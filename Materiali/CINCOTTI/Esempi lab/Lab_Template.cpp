#include <iostream>
#include <typeinfo>
#include <cstdlib>
#include <string>
#define DIM 50

using namespace std;

class A
{
    protected:
        int x;

    public:
        A(int _x){x=_x;}
        virtual int f() const =0;

       virtual ostream &put(ostream &os)
        {
            return os<< "x="<< x << " ";
        }

};

        ostream & operator << (ostream & left, A &obj)
         {
             return obj.put(left);
         }

class B:public A
{
    private:
        char b;

    public:                             /**Il dubbio � qui!**/
    //    B(char _b, int _x) :A(_b){b=_x;}/**se assegno  :A(_x){b=_b;} da errore**/

        B(int _x, char _b) : A(_x){b=_b;}
        
        int f() const
        {
            if(x %2==0)return (int)b;

            return((int)b) *2;
        }

        ostream &put(ostream & os){ return A::put(os)<<" b='"<< b<<"'";}
};



template <class T>
class C:public A
{
    private:
        T c;

    public:
        C(int _x, T _c) :A(_x){  this->c=_c;}

        int f() const {return x+20*c;}

        T const f(double eps)
        {
            if(typeid(c)==typeid(char)) return (char)((int)c+1);

            return f()+eps;
        }

        ostream &put(ostream &os){ return A::put(os)<< " c="<< c;}

};

int main ()
{
    srand(328832748);
    A * vett[DIM];
    string conc=""; int max_val=0;

    for(int i=0; i<DIM; i++)
    {
        int x=1+rand()%100;
        int c='a'+rand()%26;

        switch(rand()%3)
        {
            case 0: vett[i]= new B(x,c); break;
            case 1: vett[i]= new C<double>(x,rand()/(double)RAND_MAX);break;//
            case 2: vett[i]= new C<char>(x, c+1);

        }
    }
    for(int i=0; i<DIM; i++)
    {
       if(max_val<(*vett[i]).f()) { max_val=(*vett[i]).f();}
        cout<<i<<")"<<*vett[i];
        cout<<" f()="<<(*vett[i]).f();

        if(typeid(*vett[i])==typeid(C<double>))
        {
            cout<<" f(0.03)="<<((C<double>*)vett[i])->f(0.03);
        }
        cout<<endl;

        if(typeid(*vett[i])==typeid(C<char>))
        {
            conc+=((C<char>*)vett[i])->f(0.03);
        }
    }

    cout<<"\nmax = "<<max_val;
    cout<<"\nconc = "<<conc;
}
