#include <iostream>
#include <iomanip>


#include <cmath>


using namespace std;


int main()
{
   int unNumeroTipoInt = 234918743;   
       //questo � un intero a 4 byte?		
			 
   long long unNumeroGrande = 100000000000000000LL; 
       //� necessario il suffisso LL
			 
   short int unNumeroCorto= 30000;		
       //questo richiede solo 2 byte ?
   
   signed char unChar = 78;               
       // questo richiede solo un byte !
   
   cout << "short int  -> " << sizeof (short int) << endl;
   cout << "int        -> " << sizeof (int) << endl;
   cout << "long int   -> " << sizeof (long int) << endl;
   cout << "long long  -> " << sizeof (long long) << endl;
   cout << "float      -> " << sizeof (float) << endl;
   cout << "double     -> " << sizeof (double) << endl;
   cout << "long double-> " << sizeof (long double) << endl; 
   cout << "bool       -> " << sizeof (bool) 
        << endl << endl;

   cout << "unNumeroTipoInt = " << unNumeroTipoInt << endl;
   cout << "unNumeroGrande  = " << unNumeroGrande << endl;
   cout << "unNumeroCorto   = " << unNumeroCorto << endl << endl;
 
 
 
   // Costanti intere 
 
   int x = 75;     // decimale
   cout << " x = " << x << endl; 
   x = 0113;       // ottale
   cout << " x = " << x << endl; 
   x = 0x4b;       // esadecimale
   cout << " x = " << x << endl << endl; 
 
 
   // Gestione dell'overflow per int e float 
 
   cout << "max=" << INT_MAX << " max+1= " << INT_MAX+1 << endl;  
   
   cout << "Un risultato int molto grande : " 
        << unNumeroTipoInt * unNumeroTipoInt << endl;
    
   float  unNumeroFloat = 1.34E27;
   
   cout << "unNumeroFloat = " << unNumeroFloat << endl;
   
   cout << "Un risultato float molto grande : " 
        << unNumeroFloat * unNumeroFloat << endl << endl;



   // Errori di rappresentazione

   double price = 4.35;
   int cents = 100 * price; // Should be 100 * 4.35 = 435
   cout << cents << endl;  // Prints 434!
   
       // non � sempre quel che si vede ... 
   cout << 3.45 << " con maggiore precisione ... " 
        << setprecision(20) << 3.45 << endl << endl; 
        
   cents = 100000000 * price;
   double aux = 100000000 * price;
   cout << setprecision(5) << cents << "   " << aux << endl; 
   cout << fixed << setprecision(20) << cents 
        << "   " << aux << endl << endl; 

    
    
   // Errori di approssimazione 
    
   double r = sqrt(2.0);			
   cout << "sqrt(2) squared is not 2 but "
      << setprecision(20) << r * r << endl;


    system("PAUSE");    // ns. comodo!
    return 0;
} // end main()
