#include <iostream>
#include "serbatoio.cpp"

using namespace std;


int main()
{
   Serbatoio a(10,7);
   Serbatoio b(20);
 
   cout << a << endl << b;

    b.deposita(a.svuotaTutto());
	  
    cout << "Dopo il riversamento ..." << endl;
	  
    cout << a << endl << b;
    
    system("PAUSE");
    return 0;
} // end main()
