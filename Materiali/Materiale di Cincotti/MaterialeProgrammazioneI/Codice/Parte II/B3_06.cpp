#include <iostream>
#include "serbatoio.cpp"

using namespace std;


int main()
{
    Serbatoio a(100,100);
    Serbatoio b(100,0);
    Serbatoio c(80);

    int tempoTrascorso = 0;     

    while (!c.pieno())
      {
        cout << "INIZIO TRANSAZIONI" << endl;
         
        float uno = a.preleva(a.getQuantita()/5);
        b.deposita(uno); 
         
        float due = a.preleva(a.getQuantita()/10);
        c.deposita(due); 

        float tre = b.preleva(b.getQuantita()/2);
        c.deposita(tre);
         
        float quattro = b.preleva(b.getQuantita()/2);
        a.deposita(quattro);
         
        cout << a << endl << b << endl << c << endl;
         
        tempoTrascorso += 2;
      } 
 
        cout << "Tempo trascorso : " << tempoTrascorso  
             << " ore.\n\n";
    
    system("PAUSE");
    return 0;
} // end main()
