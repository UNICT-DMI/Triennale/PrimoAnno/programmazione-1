#include <iostream>
#include "moneta.cpp"
#include <cstdlib>
#include <ctime>

srand(time(0))
Moneta::Moneta()
{
    effettuaLancio(); 
}

void Moneta::effettuaLancio ()
   {
     short faccia = (short) (rand(0)%2);

      if (faccia == 0) ultimaFaccia = 'T';
                  else ultimaFaccia = 'C';
   }

bool Moneta::testa() const
   {
      if (ultimaFaccia == 'T')
         return true;
      else 
         return false;
   }
   
bool Moneta::croce() const
   {
      return  !testa();
   }
   
char Moneta::getFaccia () const
   {
      return  ultimaFaccia;
   }
   
      
ostream &operator<< (ostream &stream, Moneta m)
   {
      if (m.ultimaFaccia == 'T')
           stream << "TESTA" << endl;
      else
           stream << "CROCE\n";
      return stream;     
   }
   

