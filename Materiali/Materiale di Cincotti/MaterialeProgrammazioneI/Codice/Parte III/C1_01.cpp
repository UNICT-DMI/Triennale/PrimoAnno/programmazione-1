#include <iostream>

using namespace std;

//**************
class Distanza {
//**************

public:
    // Costruttore assente 
       
    void setDistanza (int d)   
        { distanza = d; }			 

    int getDistanza ()   { return distanza; }			 
		 
    float calcolaVelocita (int tempo)
       { 
		 float velocita;
         cout << "Siamo dentro al metodo\n";
         velocita = (float)distanza / tempo;
		 tempo = 0;  // Attenzione a questo!
         cout << "Penultima istruzione del metodo\n";
         return velocita;
         cout << "Ha senso ?";
       }
       
private:
        
    int distanza;
    
}; // End class Distanza		 
		 

int main()
{
    Distanza d;
    cout << "Costruttore di default: "
         << d.getDistanza() << endl;

    // Il compilatore crea un costruttore di default 
    // con un corpo vuoto e senza inizializzatori
     
    int x=2;
    d.setDistanza(100);
    cout << "x=" << x 
         << "\t velocit� = " << d.calcolaVelocita(x) 
         << "\t x=" << x << endl;
    
    system("PAUSE");
    return EXIT_SUCCESS;
} // End main()
