#include <iostream>
#include <cmath>


using namespace std;

//**************
class Cilindro {
//**************

public:
    // Costruttore assente 
       
    void setValori (float r, float a)
	  {
	    raggioBase = r;
	    altezza = a;			
	  }
	  
	float getRaggio ()
	  {  
		return  raggioBase;
	  }

    float getAltezza ()
	  {  
		return  altezza;
	  }
	  
	float areaBase ()
	  {  
	    float r = getRaggio();
	    return  ( (float)M_PI * r * r );
	  }  // perch� getRaggio() e non raggioBase?

	float  volume ()
	  {  
		return  ( getAltezza() * areaBase() );
	  }
	  
	float  densita (float massa)
	  {  
		return  ( massa / volume() );
	  }
	  
	  
	void visualizza ()
	  {
		float massa = 5.3F;  
		cout << "Il cilindro ha :\n";
		cout << "  Raggio base = " << getRaggio();
		cout << "\t Altezza = " << getAltezza();
		cout << "\tArea base = " << areaBase() << endl;
		cout << "  Volume = " << volume();
		cout << "\tMassa = " << massa  
             << "\t Densit� = " << densita(massa) << endl;
	  }

private:
        
   	float raggioBase;
	float altezza;
    
}; // End class Cilindro		 
		 

int main()
{
    Cilindro a;

    a.setValori(1, 5);
    a.visualizza ();
	   
    a.setValori((long)1, 5);
    a.visualizza ();

    a.setValori((int)1.3, 5);
    a.visualizza ();

    a.setValori((float)1.1, 5.0F);
    a.visualizza ();  

    
    system("PAUSE");
    return EXIT_SUCCESS;
} // End main()
