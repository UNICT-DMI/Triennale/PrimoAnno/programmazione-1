//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : C1_04.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Funzionalita' basilari
//***   Argomento      : Parametri da linea di comando
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************

#include <iostream>


using namespace std;


int main(int argc, char *argv[])
{
  cout << " argc �: " << argc << endl;
  cout << " il nome dell'eseguibile � " << *argv << endl; 
       // << argv[0] << endl;
 
   for (int i=1; i<argc; i++)
      cout<<"Argomento #"<<i<<" = " << *(argv+i) << endl;
       // <<argv[i] << endl; 
          
    system("PAUSE");
    return EXIT_SUCCESS;
} // End main()
