//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : C1_06.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Funzionalita' basilari
//***   Argomento      : Le funzioni "inline"
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


inline int f (int n)
  {
    int k = n;

    k = 3*k;
    if (k<100)   return k;
	  else   return k-100;
  }


inline int fatt (int n)       // Non viene considerata "inline"
  {
    if (n > 0) return (n * fatt(n-1));
    else return 1;
  }


int main()
  {
    int i;

    cout << "Inserisci un intero : ";
    cin >> i;
    cout << fatt(i) << "   " << f(i) << endl;
   
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }

//*****************************************************************
//*****************************************************************
