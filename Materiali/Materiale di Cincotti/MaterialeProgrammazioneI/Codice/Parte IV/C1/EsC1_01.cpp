//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC1_01.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : I membri delle classi
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Coordinate
  {
    private :
       double x;
       double y;

    public:
       void Inizializza (double xx = 0.0,   double yy = 0.0);
       char * Visualizza (void);
  };


class Coordinate_Bis   // Dichiarazione equivalente alla precedente
  {
       double x;

    public:
       void Inizializza (double xx = 0.0,   double yy = 0.0);

    private :
       double y;

    public:
       char * Visualizza (void);
  };



int main()
  {
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }

//*****************************************************************
//*****************************************************************
