#include <iostream>

#include "carattereI.h"
#include "carattereII.h"
#include "carattereIII.h"


using namespace std;


int main()
{ 
   CarattereI   a;
   CarattereII  b;
   CarattereIII c;
		
   a.setCarattere('*');
   a.visualizzaUno();
   cout << endl << endl;
   a.visualizzaDieci();
   cout << endl << endl;
   a.visualizzaN(20);
   cout << endl << endl;
   a.visualizzaRigheDa10(4);
   cout << endl;
   a.visualizzaRigheDaN(2, 30);
   cout << endl;
		
		
   b.setCarattere('$');
   b.visualizzaUno();
   cout << endl << endl;
   b.visualizzaDieci();
   cout << endl << endl;
   b.visualizzaN(20);
   cout << endl << endl;
   b.visualizzaRigheDa10(4);
   cout << endl;
   b.visualizzaRigheDaN(2, 30);
   cout << endl;		


   c.visualizzaUno('#');
   cout << endl << endl;
   c.visualizzaDieci('#');
   cout << endl << endl;
   c.visualizzaN('#', 20);
   cout << endl << endl;
   c.visualizzaRigheDa10('#', 4);
   cout << endl;
   c.visualizzaRigheDaN('#', 2, 30);
   cout << endl;   

   system("PAUSE");
   return EXIT_SUCCESS;
} // End main()
