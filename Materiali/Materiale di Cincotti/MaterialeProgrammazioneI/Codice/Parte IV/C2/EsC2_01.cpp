//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC2_01.cpp
//***   Data creazione : 06/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Le funzioni membro "const"
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Libro
  {
    private :

       int pagine;
       float costo;

    public:

       void Inizializza (int, float);
       void Visualizza  (void)        const;
       int  f1          (float)       const;

       float f2 () const
	  {
	    return  costo / pagine;
	  }       // Implicitamente inline

  }; //End class Libro


void Libro::Inizializza (int pagine, float p) 
  {
    Libro::pagine = pagine;
    costo = p;

    // Se questa funzione membro fosse dichiarata "const"
    // gli assegnamenti non sarebbero validi
  }


void Libro::Visualizza (void) const
  {
    cout << "pag. " << pagine << "   costo : "
	 << costo << " euro\n";
  }


int Libro::f1 (float coeff) const
  {
    coeff *= pagine;
    return (int) (coeff + costo);
  }


int main ()
  {
    Libro a;

    a.Inizializza (337, 550.5);
    cout << "Intero : " << a.f1(.1)
	     << "    Float : " << a.f2() << '\n';
    a.Visualizza ();
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;

  }



//*****************************************************************
//*****************************************************************
