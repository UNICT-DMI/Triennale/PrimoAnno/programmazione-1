//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC2_04.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi     
//***   Argomento      : I puntatori a funzioni 
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it 
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;



unsigned int f(const char * stringa)
   {
      return stringa[0];    
   }


unsigned int g(const char * stringa)
   {
      return stringa[strlen(stringa)-1];    
   }


int main ()
  {
   unsigned int (* ptr_funzione) (const char *);
         
   ptr_funzione = f;
   cout << (*ptr_funzione) ("Alba") << '\n';

   ptr_funzione = g;
   cout << (*ptr_funzione) ("Alba") << '\n';

   ptr_funzione = strlen;
   cout << (*ptr_funzione) ("Ecco la prova") << '\n';
           
   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
