//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC2_07.cpp
//***   Data creazione : 06/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Oggetti come parametri di funzioni (II)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>
#include <string>

using namespace std;


class Libro
  {
    private :

       char   titolo[80];
       int    pagine;
       char * editore;
       float  costo;

    public:

       void Init_Costo (float c)  { costo = c; }
       void Init_Libro (Libro *);
       void Visualizza ();

  }; //End class Libro


void Libro::Init_Libro (Libro * l)
  {
    strcpy (l -> titolo, "Oggetto passato come parametro : ");
    l -> pagine = 87;
    l -> editore = (char *) NULL;
    l -> Init_Costo (22.99);

    strcpy (titolo, "Oggetto cui viene inviato il messaggio : ");
    pagine = 254;
    editore = (char *) NULL;
    Init_Costo (34.50);
  }


void Libro::Visualizza ()
  {
    cout << titolo  << "  Pag. " << pagine
	 << "  Costo : " << costo << " euro\n";
  }



int main ()
  {
    Libro x, y;

    x.Init_Libro (& y);

    x.Visualizza ();
    y.Visualizza ();

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()




//*****************************************************************
//*****************************************************************
