//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC3_01.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Inizializzazione primitiva
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Persona
  {
    public:

       int   eta;
       float altezza;

  }; //End class Persona


int main()
  {
    const Persona pippo = {63, 1.76};

    Persona franco = {61, 1.72};

    Persona clienti[4] = { {33, 1.74},
			   {65, 1.75},
			   {23, 1.76},
			   {37, 1.77}
			 };

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()


//*****************************************************************
//*****************************************************************
