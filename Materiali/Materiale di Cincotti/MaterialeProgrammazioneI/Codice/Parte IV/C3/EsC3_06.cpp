//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC3_06.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Dichiarazione di oggetti
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;



class Reale
  {
    private :

       float Re;

    public:

       Reale (float a)
	  {
	    cout << "Costruttore\n";
	    Re = a;
	  }

  }; //End class Reale


int main ()
  {
    Reale x(2.7);            // Forma migliore e piu' efficiente
    Reale y = Reale(2.7);
    Reale w = 2.7;           // Chiamata implicita al costruttore
    Reale z = (Reale) 2.7;   // Chiamata implicita al costruttore

    // Anche con tipi di dati primitivi

    // int a(2);    Nuova funzionalita'
    int b = int(2);
    int c = 2;
    int d = (int) 2;

    cout << "Inizio\n";
    Reale xx = x;         // Non viene chiamato alcun costruttore
    cout << "Fine\n";

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()


//*****************************************************************
//*****************************************************************
