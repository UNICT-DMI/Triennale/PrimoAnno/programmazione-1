//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC3_10.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Inizializzazione di attributi (III)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Classe
  {
    private :

       const int c;
       int i;
       float * f;
       double & d;

    public:

       Classe (int, int, float *, double &);

       void Visualizza () const;

  }; //End class Classe


Classe::Classe (int cc, int ii, float * ff, double & dd) :
	c(cc),  i(ii),  f(ff),  d(dd)
  {
    // E' preferibile inizializzare le variabili membro
    // nella lista di inizializzazione
    // Le costanti e gli indirizzi devono essere inizializzati
    // Gli array costituiscono un'eccezione
  }


void Classe::Visualizza () const
  {
   cout << "Costante : " << c << '\n';
  }



int main ()
  {
    float  x = 2.3;
    double y = 0.344;

    Classe c1(10, 8, &x, y);
    Classe c2(20, 8, &x, y);

    c1.Visualizza ();
    c2.Visualizza ();

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()



//*****************************************************************
//*****************************************************************
