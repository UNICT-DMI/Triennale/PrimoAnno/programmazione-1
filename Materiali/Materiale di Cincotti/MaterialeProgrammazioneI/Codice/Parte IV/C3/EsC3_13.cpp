//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC3_13.cpp
//***   Data creazione : 12/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Costruttore di copia (III)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>
//#include <string>

using namespace std;


class Messaggio
  {
    private :

       char * testo;
       char * mittente;
       int    priorita;

       void Set (const char *, const char *, int);

    public:

       Messaggio (const Messaggio &);   // Costruttore di copia
       Messaggio (const char *, const char * = "", int = 0);

       void Virus (void);
       void Visualizza (void) const;

  }; //End class Messaggio


void Messaggio::Set (const char * t, const char * m, int p)
  {
    testo = new char [strlen(t)+1];
    mittente = new char [strlen(m)+1];
    strcpy (testo, t);
    strcpy (mittente, m);
    priorita = p;
  }


Messaggio::Messaggio (const Messaggio & mess)
  {
    Set (mess.testo, mess.mittente, mess.priorita);
    cout << "Costruttore di copia\n";
  }


Messaggio::Messaggio (const char * t, const char * m, int p)
  {
    Set (t,m,p);
    cout << "Costruttore\n";
  }


void Messaggio::Virus (void)
  {
    for (int i=0; i<strlen(testo); i++)   testo[i]++;
  }


void Messaggio::Visualizza (void) const
  {
    cout << "Mitt.: " << mittente << "  Mess.: " << testo
	 << "  Liv. priorit… : " << priorita << '\n';
  }



int main (void)
  {
    Messaggio a("Buongiorno !!!", "Pippo",3);
    Messaggio b = a;

    cout << "Prima ...\n";
    a.Visualizza ();
    b.Visualizza ();

    cout << "Dopo ...\n";
    a.Virus ();
    a.Visualizza ();
    b.Visualizza ();

    cout << "Infine ...\n";
    b = a;           // Non viene chiamato il costruttore di copia !
    a.Visualizza ();
    b.Visualizza ();

    // Domanda : Dove viene liberata la memoria allocata precedentemente ?
   
   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
