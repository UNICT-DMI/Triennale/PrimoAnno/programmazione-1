#include <iostream>

using namespace std;


 // scambia due elementi di un array
static void swap ( int a[], int primo, int secondo )
   {
      int tmp;
      
      tmp = a[primo];         
      a[primo] = a[secondo];  
      a[secondo] = tmp;
   }
   
   
// BubbleSort
static void bubbleSort ( int array[], int length )
  {   
    for ( int pass = 1;  pass < length; pass++ ) 
         for ( int i = 0; i < length-1; i++ )   
            if ( array[i] > array[i+1] )    
               swap( array, i, i+1 );       
   }
   
// Miglioria : for interno "-1" -> "-pass"


int main(){
	
	int N = 8;  
   int numeri[N] = { 8, 4, 6, 1, 2, 7, 5, 3 };

   bubbleSort( numeri, N );
      
   for (int i=0; i < N; i++)
        cout << numeri[i] << "\t";
   cout << endl;

   system("PAUSE");    // ns. comodo!
   return 0;
} // End main()
