//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : D5_01.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Funzionalita' basilari
//***   Argomento      : Allocazione dinamica
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


int main()
  {
    int *   iptr;
    char *  cptr;
    float * fptr;

    iptr = new int;         // L'operatore new restituisce void* che
    cptr = new char[80];    // viene automaticamente convertito
    fptr = new float[30];

    *iptr = 10;

    *cptr   = 'A';
    cptr[1] = 'B';
    cptr[2] = '\0';

    *fptr = 5.34;

    cout << *iptr << "  " << *(cptr+1) << "  " << fptr[0] <<'\n';
    cout << cptr << '\n';

    delete iptr;
    delete [] cptr;
    delete [] fptr;
   
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }
//
//  Attenzione :
//
//  if ((ptr = new double[5*x]) == NULL)   GestisciErrore();
//

//*****************************************************************
//*****************************************************************
