//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsD1_01.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Overload di operatori
//***   Argomento      : Operatori non-membro
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>
//#include <stdlib.h>

using namespace std;

class Vettore3D
  {
    private :

       float    x, y, z;

    public:

       // Vettore3D ()   { x = y = z = 0.0; }
       Vettore3D (float l=0.0, float m=0.0, float n=0.0) :
	       x(l), y(m), z(n)  
           { cout << " COS "; }

       float Prima   () const {return x;}
       float Seconda () const {return y;}
       float Terza   () const {return z;}

       void Visualizza (void) const
	   { cout << "( " << x << ", " << y << ", " << z << " )"; }

  }; //End class Vettore3D


// Operatore binario
Vettore3D operator + (const Vettore3D sx, const Vettore3D dx)
  {
    return  Vettore3D (sx.Prima()   + dx.Prima(),
		               sx.Seconda() + dx.Seconda(),
		               sx.Terza()   + dx.Terza()
                      );
  }


// Operatore unario
Vettore3D operator - (const Vettore3D operando)
  {
    return  Vettore3D (-operando.Prima(),
		               -operando.Seconda(),
		               -operando.Terza()
		               );
  }


// Operatore binario con argomenti di tipo diverso
// Non necessario poich� interviene int->Vettore3D
Vettore3D operator * (const int coeff, const Vettore3D dx)
  {
    return  Vettore3D (coeff * dx.Prima() ,
		               coeff * dx.Seconda(),
		               coeff * dx.Terza()
		               );
  }


// Operatore binario "*" overloaded
// Non necessario poich� interviene int->Vettore3D
Vettore3D operator * (const Vettore3D sx, const int coeff)
  {
    return  Vettore3D (coeff * sx.Prima() ,
		               coeff * sx.Seconda(),
		               coeff * sx.Terza()
		               );
  }


// N.B. : A volte gli operandi DEVONO essere passati per riferimento
//        e possono essere non-const


int main ()
  {
    Vettore3D a (1,2,3);
    Vettore3D b = -a;
    Vettore3D c;

    cout << "\n\n";

    (a+b).Visualizza();     cout << "  =  ";
    c = operator + (a,b);
    c.Visualizza();         cout << "\n\n";

  c=a+b;


    b = 3 * a;
    b.Visualizza();         cout << "  =  ";
    (a * 3).Visualizza();   cout << "\n\n";

    // DOMANDA : E' lecito scrivere quanto segue ?

    b = (30.7 + a);    b.Visualizza();
    cout << "\n\n";
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
