//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsD1_09.cpp
//***   Data creazione : 08/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Overload di operatori
//***   Argomento      : Conversione di tipo (I)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Complex
  {
    private :

       float Re;
       float Im;

    public:

       Complex (float a = 0.0, float b = 0.0) :  
           Re(a), Im(b)
	       { cout << "Costruttore\n"; }


       Complex (const Complex & c) :  
           Re(c.Re), Im(c.Im)
	       { cout << "Costruttore di copia\n"; }


       Complex operator + (const Complex & dx)
	  {
	    return Complex(Re+dx.Re,  Im+dx.Im);
	  }


       void Visualizza (void) const
	  { cout << Re << "+" << Im << "i\n"; }

  }; //End class Complex




int main ()
  {
    Complex a(2.4, 3.7);
    Complex b = a;

    a.Visualizza ();
    b.Visualizza ();

    b = Complex(2.0);          // Conversione esplicita
    b = (Complex) 2.0;         // Conversione esplicita
    b = 2.0;                   // Conversione implicita

    cout << "-------------\n";
    b = a + (Complex) 3.2;     // Conversione esplicita
    b = a + 3.2;               // Conversione implicita
    b = a + 5;                 // Doppia conversione implicita

    // b = 3.2 + a;    Errore di compilazione ...
    cout << "\n\n";
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;    
  }


//*****************************************************************
//*****************************************************************
