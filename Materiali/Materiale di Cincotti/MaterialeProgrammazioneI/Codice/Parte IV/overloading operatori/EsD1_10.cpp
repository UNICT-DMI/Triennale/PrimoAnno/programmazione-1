//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsD1_10.cpp
//***   Data creazione : 08/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Overload di operatori
//***   Argomento      : Conversione di tipo (II)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Complex
  {
    private :

       float Re;
       float Im;

    public:

       Complex (float a = 0.0, float b = 0.0) :  
            Re(a), Im(b)
	        { cout << "Costruttore\n"; }


       Complex (const Complex & c) :  
            Re(c.Re), Im(c.Im)
	        { cout << "Costruttore di copia\n"; }


       operator double () const   
         { 
           cout << "overload cast\n";
           return  Re; 
         }


       void Visualizza (void) const
	  { cout << Re << "+" << Im << "i\n"; }

  }; //End class Complex




int main ()
  {
    int i = 2;
    double x = 3.3;
    Complex c(2.4, 3.7);

    x = x + c;
    cout << "x = " << x << '\n';

    x = c * i;
    cout << "x = " << x << '\n';
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
