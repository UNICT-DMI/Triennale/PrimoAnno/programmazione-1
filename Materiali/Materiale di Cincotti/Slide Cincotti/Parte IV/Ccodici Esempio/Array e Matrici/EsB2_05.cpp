//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsB2_05.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Funzionalita' basilari
//***   Argomento      : I riferimenti (I)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;



int main()
  {
    int  libro, *ptr;
    int &tomo = libro;

    tomo = 100;   // Equivalente a : libro = 100;
    ptr = &tomo;  // Equivalente a : ptr = &libro;

    printf ("%d %d\n", libro, tomo);
   
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }

//
// Le variabili indirizzo vengono tradotte con puntatori
// In particolare : tomo  -> *v
//
//    int  libro, *ptr;
//    int * const v = &libro;
//
//    *v = 100;
//    ptr = v;   // In quanto  &tomo = &*v = v
//
//    printf ("%d %d\n", libro, *v);
//


//*****************************************************************
//*****************************************************************
