#include <iostream>
 
using namespace std;
 
const int DIM = 7; 
 

// Array STATICO


int main()
{ 
   float a[DIM];
   for(int i=0; i<DIM; i++)
      {
        a[i] = i+1;
        cout << a[i] << "  ";
      }
   cout << endl;
   
   // nel caso monodimensionale un array � un puntatore al tipo base
   // il nome del vettore � un puntatore costante  
   
   float * b;
   b=a;
   //  a=b;    Errore: "a" non si pu� modificare

   b[3] = 0;
   
   for(int i=0; i<DIM; i++) {  
      cout << a[i] << " ";
      cout << *(a+i) << " ";  // si pu� usare la notazione a puntatore 
      cout << b[i] << " ";    // oppure la notazione con indice
      cout << *(b+i) << " ";  
   }
   cout << endl;
       
   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
