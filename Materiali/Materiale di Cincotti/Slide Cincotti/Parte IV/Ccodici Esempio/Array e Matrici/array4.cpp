#include <iostream>
 
using namespace std;

// Array DINAMICO

void StampaArray(float * a, int dim)    // anche  "float a[]"
{
   for (int i=0;i<dim;++i)
      {
        cout << a[i] << " ";
        cout << *(a+i) << " ";
      }
   cout<<endl;
}


int main()
{ 
   int dim=7;
  
   float *m;// puntatore al tipo base

   // la seguente implementazione crea l'array dinamico
    
   m=new float [dim];  // un array di "dim" elementi
   
   for(int i=0; i<dim; i++)
      {
         m[i]=i+1;     // analogamente  "*(m+i)"
         cout << m[i] << "  ";  // � possibile usare la notazione con indice
      }
   cout << endl;

   StampaArray(m, dim); 

   cout << m << "  " << *m << "  " << &m[0] << endl << endl;
   
   delete [] m;
       
   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
