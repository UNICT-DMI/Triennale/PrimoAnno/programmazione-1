#include <iostream>
 
using namespace std;


// Matrice DINAMICA



int ** CreaMatrice(int righe,int colonne)
{  
   int **m=new int*[righe];  // un array di array di "righe" elementi
   
   for (int i=0;i<righe;++i)   // creazione di "righe" array di dimensione "colonne"
       m[i]=new int[colonne];  // le colonne potrebbero variare da riga a riga!

   for (int i=0;i<righe;++i)
   {
      for (int j=0;j<colonne;++j)
      {
         m[i][j]=i*colonne+j+1;
      }
   }
   return m;
}


void EliminaMatrice (int **m, int righe)
{
   for (int i=0;i<righe;++i)   // libera la memoria riga per riga
       delete [] m[i];        
   delete [] m;                // libera l'array dei puntatori alle righe     
}


void StampaMatrice(int **mat,int righe,int colonne)
{
   for (int i=0;i<righe;++i)
   {
      for (int j=0;j<colonne;++j)
      {
         cout<<mat[i][j]<<" ";
      }
      cout<<endl;
   }
}


int main()
{ 
   int righe=3;
   int colonne = 4;  
   int **matrice;// puntatore doppio

   // la seguente implementazione crea dinamicamente la matrice riga per riga,
   // ed � totalmente diversa dalla rappresentazione 
   // classica delle matrici in C/C++
    
   matrice = CreaMatrice(righe, colonne);
   matrice[1][2]=0;
   StampaMatrice(matrice, righe, colonne); 
   EliminaMatrice(matrice, righe);
   
   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
