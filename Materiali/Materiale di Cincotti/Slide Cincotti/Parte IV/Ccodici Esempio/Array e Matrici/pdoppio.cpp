#include <iostream>
 
using namespace std;
 


class Quadrato { 
private:
    int lato;
public:
    Quadrato(int s = 1)  { lato = s; }
    int getLato()        { return lato; }
};


void swap_C (Quadrato ** p1, Quadrato ** p2)
{
  Quadrato * aux;
  aux = *p1;
  *p1=*p2;
  *p2=aux;   
}
   

void swap_CPP (Quadrato* & p1, Quadrato* & p2)
{
  Quadrato * aux;
  aux = p1;
  p1=p2;
  p2=aux;   
}


int main () {
   Quadrato *s1, *s2;
   
   s1 = new Quadrato(3);
   s2 = new Quadrato(7);
   
   cout << s1->getLato() << "  " << s2->getLato() << endl;
 
   swap_C (&s1, &s2);   
   cout << s1->getLato() << "  " << s2->getLato() << endl;

   swap_CPP (s1, s2);   
   cout << s1->getLato() << "  " << s2->getLato() << endl;
       
   delete s1, s2;
       
   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
