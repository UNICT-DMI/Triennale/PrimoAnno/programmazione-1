//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC1_02.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Le variabili membro
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Dipendente
  {
    private :

       char * nome;
       char   indirizzo[80];
       double stipendio;
       int    codice;

       int f (int j, double x);

    public:

       void Inizializza (const char *, const char *, double, int);
       char * Visualizza (void);

  } pippo;



int main()
  {
   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
  }

//*****************************************************************
//*****************************************************************
