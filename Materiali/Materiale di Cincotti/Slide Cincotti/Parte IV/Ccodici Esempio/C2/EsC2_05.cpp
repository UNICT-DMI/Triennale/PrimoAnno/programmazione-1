//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC2_05.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : I puntatori a funzioni membro (II)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it 
//***
//*****************************************************************
//*****************************************************************


#include <iostream>
#include <string>

using namespace std;


class Stringa
  {  
    private :

       char str[80];

    public:

       void Set (const char * s)
	  {
	    strcpy (str, s);
	  }

       void Get (char * buf) const
	  {
	    strcpy (buf, str);
	  }

  }; //End class Stringa


// In alternativa :
//     typedef void (Stringa::*TIPO_PTR_FUNZIONE) (char *) const;

int main ()
  {
    char buffer[80];
    Stringa *ptr, st;
    void (Stringa::*ptr_funzione_membro) (char *) const;

    ptr = &st;

    ptr_funzione_membro = & Stringa::Get;   // In alcuni compilatori
					    // la & non � necessaria.

    // In alternativa :
    //     TIPO_PTR_FUNZIONE  ptr_funzione_membro = & Stringa::Get;

    st.Set ("Primo");
    (st.*ptr_funzione_membro) (buffer);      // st.Get (buffer);
    cout << buffer << '\n';

    st.Set ("Secondo");
    (ptr ->* ptr_funzione_membro) (buffer);  // st.Get (buffer);
    cout << buffer << '\n';

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()



//*****************************************************************
//*****************************************************************
