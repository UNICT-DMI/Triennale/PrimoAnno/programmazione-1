//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC2_08.cpp
//***   Data creazione : 06/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : La relazione di composizione (I)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>
#include <string>

using namespace std;


const int Dim = 30;

class Lavoro
  {
    private :

       char Descrizione[Dim];

    public:

       void Set (const char * s)
	  {
	    strcpy (Descrizione, s);
	  }

       void Get (char * buf) const
	  {
	    strcpy (buf, Descrizione);
	  }

  }; //End class Lavoro


class Lavoratore
  {
    private :

       char nome[Dim];
       Lavoro occupazione;  // La classe "Lavoro" deve essere
			    // definita precedentemente

    public:

       void Set (const char * nm, const char * occ)
	  {
	    strcpy (nome, nm);
	    occupazione.Set (occ);
	  }

       void Visualizza (void) const
	  {
	    char buffer[Dim];
	    occupazione.Get (buffer);
	    cout << nome << "    " << buffer;
	  }

  }; //End class Lavoratore



int main ()
  {
    Lavoratore x;

    x.Set ("Pippo", "Impiegato\n");
    x.Visualizza ();

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()



//*****************************************************************
//*****************************************************************
