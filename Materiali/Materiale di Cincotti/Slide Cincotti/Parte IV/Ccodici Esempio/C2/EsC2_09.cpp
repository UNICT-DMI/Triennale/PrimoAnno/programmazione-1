//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC2_09.cpp
//***   Data creazione : 06/11/2014
//***   Progetto       : Laboratorio di Algoritmi "C++"
//***   Lezione        : Le classi
//***   Argomento      : La relazione di composizione (II)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>
#include <string>

using namespace std;


const int Dim = 30;

class Lavoro;       // Dichiarazione forward;

class Lavoratore
  {
    private :

       char nome[Dim];
       Lavoro * occupazione;   // La classe "Lavoro" DEVE essere
			       // almeno dichiarata precedentemente

    public:

       // ...

  }; //End class Lavoratore



class Lavoro
  {
    private :

       char Descrizione[Dim];

    public:

       void Set (const char * s)
	  {
	    strcpy (Descrizione, s);
	  }

       void Get (char * buf) const
	  {
	    strcpy (buf, Descrizione);
	  }

  }; //End class Lavoro



int main ()
  {
    Lavoratore x;

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()



//*****************************************************************
//*****************************************************************
