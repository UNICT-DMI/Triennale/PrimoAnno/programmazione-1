//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC2_12.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Funzioni membro "static"
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Classe
  {
    private :

       float peso;
       static int peso_totale;

    public:

       static int contatore_oggetti;

       void Inizializza (int p)
	  {
	    peso = p;
	    contatore_oggetti++;
	    peso_totale += p;
	  }

       static void Visualizza (void)
	  {
	    cout << "Peso : " << peso_totale
		 << "    Oggetti :" << contatore_oggetti << '\n';
	  }

       static void Decrementa (void)
	  {
	    contatore_oggetti--;
	  }

  }; //End class Classe


// Le variabili statiche DEVONO essere
// inizializzate all'esterno

int Classe::peso_totale = 0; // Costituisce un'eccezione al data-hiding
int Classe::contatore_oggetti = 0;


int main ()
  {
    Classe x,y,z;

    x.Inizializza (10);
    y.Inizializza (20);
    z.Inizializza (30);

    x.Visualizza ();
    x.Decrementa ();        // Notazione da evitare
    Classe::Decrementa ();  // Notazione pi� significativa

    cout << "contatore : " << Classe::contatore_oggetti << '\n';

    // Classe::peso_totale = 0;        Errore di compilazione

   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()

//*****************************************************************
//*****************************************************************
