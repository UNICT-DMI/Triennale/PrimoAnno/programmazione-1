//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC2_13.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Funzione globale "friend" di una classe
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Lira
  {
    private :

       int valore;

    public :

       Lira (int v)   { valore = v; }

       friend float Convertitore (float, Lira &);
	   // In genere una funzione friend possiede un
	   // parametro costituito da un oggetto della
	   // classe che la contiene

  }; //End class Lira


float Convertitore (float coeff, Lira & l)
  {
    return l.valore / coeff;     // La variabile "valore" � privata !!!
  }


int main ()
  {
    Lira diecimila(10000);

    cout << "10.000 lire equivalgono a "
	 << Convertitore (1936.27, diecimila) << " euro\n";

   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()


//*****************************************************************
//*****************************************************************
