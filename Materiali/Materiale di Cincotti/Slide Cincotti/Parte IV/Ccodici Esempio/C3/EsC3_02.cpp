//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC3_02.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Inizializzazione membro a membro
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>
#include <string>

using namespace std;


class Stringa
  {
    private :

       char str[80];

    public:

       void Inizializza (char * s)
	  {
	   strcpy (str, s);
	  }

       void Leggi (char * buf)
	  {
	   strcpy (buf, str);
	  }

  }; //End class Stringa


int main()
  {
    char buffer[80];
    Stringa s1;

    s1.Inizializza ("Prima stringa\n");

    Stringa s2 = s1;

    s2.Leggi (buffer);    cout << buffer;

    Stringa s3 = s1;
    s3.Inizializza (buffer);

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()



//*****************************************************************
//*****************************************************************
