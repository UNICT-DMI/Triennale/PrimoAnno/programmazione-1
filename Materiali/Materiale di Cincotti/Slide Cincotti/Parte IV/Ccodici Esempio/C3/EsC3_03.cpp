//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC3_03.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Costruttori
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>
#include <string>

using namespace std;


class Complex
  {
    private :

       float Re;
       float Im;

    public:

       Complex (float a, float b)
	  {
	    cout << "Costruttore (" << a << ", " << b << ")\n";
	    Re = a;
	    Im = b;
	  }

  }; //End class Complex


int main ()
  {
    Complex x(3.3, 5.0),   y(2.7, 3.14);
    Complex z = Complex(2.0,2.0);         // Poco efficiente

    system("PAUSE");
    return 0;
} // end main()


//*****************************************************************
//*****************************************************************
