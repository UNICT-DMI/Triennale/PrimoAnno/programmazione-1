//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : C4_02.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Funzionalita' basilari
//***   Argomento      : Overloading delle funzioni
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


int stampa (int x)
  {
    printf ("Intero : %d\n", x);
    return x;
  }


//  La seguente dichiarazione provoca un
//  errore di compilazione
//
// char stampa (int x)
//   {
//     ... Corpo della funzione
//   }
//


double stampa (float x)
  {
    printf ("Float  : %f\n", x);
    return (double) x;
  }


double stampa (double x)
  {
    printf ("Double : %20.18lf\n", x);
    return x;
  }



int main()
  {
    int    i = 10;
    float  x = 9.99;
    double y = .131410E-10;

    stampa(i);
    stampa(x);
    stampa(y);
   
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }

//*****************************************************************
//*****************************************************************
