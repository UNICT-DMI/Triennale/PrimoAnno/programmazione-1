#include <iostream>

using namespace std;

/* 
   Questo programma usa una funzione che 
   implementa il BubbleSort.
*/



 // scambia due elementi di un array
static void swap ( int a[], int primo, int secondo )
   {
      int tmp;
      
      tmp = a[primo];         
      a[primo] = a[secondo];  
      a[secondo] = tmp;
   }
   
   
// BubbleSort
static void bubbleSort ( int array[], int length )
  {   
    for ( int pass = 1;  pass < length; pass++ ) 
         for ( int i = 0; i < length-1; i++ )   
            if ( array[i] > array[i+1] )    
               swap( array, i, i+1 );       
   }
   
// Miglioria : for interno "-1" -> "-pass"



int main()
{ 
   int numeri[] = { 8, 4, 6, 1, 2, 7, 5, 3 };

   bubbleSort( numeri, 8 );
      
   for (int i=0; i < 8; i++)
        cout << numeri[i] << "\t";
   cout << endl;

   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
