//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsD1_12.cpp
//***   Data creazione : 08/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Overload di operatori
//***   Argomento      : Operatori di I/O
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Complex
  {
    private :

       float Re;
       float Im;

    public:

       Complex (float a = 0.0, float b = 0.0) :  
           Re(a), Im(b)
	       { cout << "Costruttore\n"; }


       Complex (const Complex & c) :  
           Re(c.Re), Im(c.Im)
	       { cout << "Costruttore di copia\n"; }


       friend ostream &  operator << (ostream & sx, const Complex & dx);
       friend istream &  operator >> (istream & sx, Complex & dx);

  }; //End class Complex


ostream &  operator << (ostream & sx, const Complex & dx)
  {
    return sx << dx.Re << "+" << dx.Im << "i";
  }


istream &  operator >> (istream & sx, Complex & dx)
  {
    char c;
    sx >> dx.Re >> c >> dx.Im >> c;
    return sx;
  }


int main ()
  {
    Complex a(2.4, 3.7), b;

    cout << a << '\n';

    cout << "Inserisci un numero complesso nella forma algebrica a+bi:\n";
    cin >> a;
    cout << '\n' << a << '\n';
    
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
