//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsB2_06.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Funzionalita' basilari
//***   Argomento      : I riferimenti (II)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;



int f (int & a, float b)
  {
    a = 100;
    return a + (int)b;
  }


void scambio (int & a, int & b)
  {
    int t = a;
    a = b;
    b = t;
  }


int main()
  {
    int  i = 47,  j = 90;

    f(i, 0.2);  // Il primo parametro deve essere "l-value"
    cout << "i = " << i << "\n\n";

    cout << "i = " << i << ",   j = " << j << '\n';
    scambio (i,j);
    cout << "i = " << i << ",   j = " << j << '\n';
   
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }

//
// I parametri indirizzo vengono tradotti con
// puntatori all'interno della funzione.
// In particolare : a  -> *ptr
//
// int f (int * const ptr, float b)
//   {
//     *ptr = 100;
//   }
//
//
// main()
//   {
//     int  i = 47;
//
//     f(&i, 0.2);
//     cout << i;
//      
//     system("PAUSE");    // ns. comodo!
//     return EXIT_SUCCESS;
//   }
//


//*****************************************************************
//*****************************************************************
