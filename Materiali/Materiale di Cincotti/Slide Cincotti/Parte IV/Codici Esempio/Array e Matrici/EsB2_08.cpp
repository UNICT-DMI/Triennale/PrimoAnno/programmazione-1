//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsB2_08.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Funzionalita' basilari
//***   Argomento      : I riferimenti (III b)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;



struct Data
  {
    int  giorno, mese, anno;
  };

Data Compleanni[] =
  {
    {17, 12, 37},
    {31, 10, 38},
    { 1,  1, 40},
    {23, 11, 42},
    { 8,  5, 44},
  };


Data & EccoLaData (int n)
  {
    return Compleanni[n];
  }


int main()
  {
    Data & d = EccoLaData(2);
    cout << d.giorno << '/' << d.mese << '/' << d.anno << '\n';

    d.mese = 8;
    cout << Compleanni[2].mese << endl;
   
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
