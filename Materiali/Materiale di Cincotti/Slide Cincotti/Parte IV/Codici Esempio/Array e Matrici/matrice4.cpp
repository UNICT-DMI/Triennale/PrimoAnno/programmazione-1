#include <iostream>
 
using namespace std;


// Matrice DINAMICA


void StampaMatrice(int **mat,int righe,int colonne)
{
   for (int i=0;i<righe;++i)
   {
      for (int j=0;j<colonne;++j)
      {
         cout<<mat[i][j]<<" ";
      }
      cout<<endl;
   }
}


int main()
{ 
   int righe=3;
   int colonne = 4;  
   int **m;// puntatore doppio

   // la seguente implementazione crea dinamicamente la matrice riga per riga,
   // ed � totalmente diversa dalla rappresentazione 
   // classica delle matrici in C/C++
    
   m=new int*[righe];  // un array di array di "righe" elementi
   
   for (int i=0;i<righe;++i)   // creazione di "righe" array di dimensione "colonne"
       m[i]=new int[colonne];  // le colonne potrebbero variare da riga a riga!

   for(int i=0; i<righe; i++)
      {
        for(int j=0; j<colonne; j++)
           {
             m[i][j]=i*colonne+j+1; // analogamente    "*(*(m+i)+j)"
             cout<<m[i][j]<<"   ";  // � possibile usare la notazione con indice
           }
        cout<<"\n";
      }
   cout << endl;

   StampaMatrice(m,righe,colonne); 

   // N.B. : i puntatori doppi sono fondamentali in questa rappresentazione!
   //        Infatti :        
   cout << m << "  " << *m << "  " << &m[0][0] << endl << endl;

   for (int i=0;i<righe;++i)   // libera la memoria riga per riga
       delete [] m[i];        
   delete [] m;                // libera l'array dei puntatori alle righe     


   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()
