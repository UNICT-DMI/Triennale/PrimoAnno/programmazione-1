//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : ___.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Funzionalita' basilari
//***   Argomento      : Modificatore di tipo "const"
//***                    applicato ai puntatori
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


int main()
{

  // Dichiarazione di variabile costante
  {
    const int i = 100;

    // i = 47;    ...   Errore di compilazione
    cout << i << '\n';
  }


  // Puntatore ad un'area di memoria di sola lettura
  {
    int j;
    const int *p = &j;

    // *p = 47;   ...   Errore di compilazione, pero' ...

    * (int *) p = 47;   // La dichiarazione "const" viene ignorata
			// con una conversione di tipo cast
    cout << j << "   " << *p << '\n';
  }


  // Dichiarazione di un puntatore costante
  {
    int i, k;
    int * const p = &i;

    // p = &k;   ...   Errore di compilazione

    *p = 90;
    cout << i << "   " << *p << '\n';
  }


  // Combinazione di puntatore "const" e puntatore a "const"
  {
    int i, k;
    const int * const p = &i;

    // *p = 47;   ...   Errore di compilazione (primo "const")
    //  p = &k;   ...   Errore di compilazione (secondo "const")
  }


  // Non si puo' aggirare una dichiarazione "const"
  {
    const int i = 47;
    int *p;

    // p = &i;   ...   Errore di compilazione, pero' ...

    p = (int *) &i;
    cout << *p << '\n';

    // Domanda : Tramite p si puo' modificare il valore di i ?
  }


   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
  }

//*****************************************************************
//*****************************************************************
