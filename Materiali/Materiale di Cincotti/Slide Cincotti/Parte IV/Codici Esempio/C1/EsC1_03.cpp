//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC1_03.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Le funzioni membro
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Stringa
  {
    private :

       char str[80];

    public:

       void Inizializza (char * s)
	  {
	   strcpy (str, s);
	  }

       void Leggi (char * buf)
	  {
	   strcpy (buf, str);
	  }

  }; //End class Stringa



int main()
  {
   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
