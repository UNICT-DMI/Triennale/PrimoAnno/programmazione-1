//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC1_06.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : L'operatore di risoluzione di "scope"
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Libro
  {
    private :

       int pagine;
       float costo;

    public:

       void Inizializza (int, float);
       void Visualizza (void);

  }; //End class Libro


void Libro::Inizializza (int pagine, float c)
  {
    Libro::pagine = pagine;
    costo = c;
  }


void Libro::Visualizza (void)
  {
    cout << "Pag. " << pagine << "    Costo : " << costo << " euro\n";
  }


double x = 23.45;


int main ()
  {
    Libro l;

    l.Inizializza (237,27.99);
    l.Visualizza ();


    double x;

    x = ::x / 2;
    cout << "\n\nGlobale : " << ::x << "    Locale : " << x << '\n';
   
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
