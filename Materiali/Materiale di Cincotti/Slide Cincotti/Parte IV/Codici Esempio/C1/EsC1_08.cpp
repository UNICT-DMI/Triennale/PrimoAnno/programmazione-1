//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC1_07.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Un primo esempio
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


const int Dim = 100;


class Coda
  {
    private :

       int array[Dim];
       int inizio, fine;

    public:

       void Init (void);
       void Enqueue (int);
       int  Dequeue (void);

  }; //End class Coda


void Coda::Init ()
   {
     inizio = fine = 0;
   }


void Coda::Enqueue (int elemento)
   {
     if (fine == Dim)
	{
	  cout << "Coda piena\n";
	  return;
	}
     fine++;
     array[fine] = elemento;
   }


int Coda::Dequeue ()
   {
     if (fine == inizio)
	{
	  cout << "Coda vuota\n";
	  return 0;
	}
     inizio++;
     return array[inizio];
   }



int main (void)
  {
    Coda a, b;

    a.Init();
    b.Init();

    a.Enqueue(3);
    b.Enqueue(10);
    a.Enqueue(4);
    b.Enqueue(20);

    cout << "Coda a : ";
    cout << a.Dequeue() << '\t';
    cout << a.Dequeue() << '\t';
    a.Dequeue();

    cout << "Coda b : ";
    cout << b.Dequeue() << '\t';
    cout << b.Dequeue() << '\t';
    b.Dequeue();
   
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
