//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC2_03.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : I puntatori a variabili membro
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Classe
  {
    private :

       int privato;

    public:

       int primo;
       int secondo;

  }; //End class Classe



int main ()
  {
    int Classe::*ptr = & Classe::primo;

    // int Classe::*ptr = & Classe::privato;
    //     Errore di compilazione

    Classe a;
    Classe * punt_ogg = & a;

    a.*ptr = 44;
    cout << punt_ogg ->* ptr << '\n';

    // Attenzione :
    //    I caratteri ".*" e "->*" devono essere contigui !!!

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()




//*****************************************************************
//*****************************************************************
