//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC2_14.cpp
//***   Data creazione : 06/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Funzione globale "friend" di pi� classi
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Euro;

class Lira
  {
    private :

       int valore;

    public :

       Lira (int v)   { valore = v; }

       friend void Convertitore (float, Lira &, Euro &);

  }; //End class Lira


class Euro
  {
    private :

       int valore;

    public :

       Euro (int v)   { valore = v; }

       friend void Convertitore (float, Lira &, Euro &);

  }; //End class Euro


void Convertitore (float coeff, Lira & l, Euro & e)
  {
    cout << l.valore / coeff << " euro  e  "
	 << e.valore * coeff << " lire\n";
  }


int main ()
  {
    Lira diecimila(10000);
    Euro dieci(10);

    cout << "10.000 lire  e  10 euro equivalgono rispettivamente a\n";
    Convertitore (1936.27, diecimila, dieci);

   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()


//*****************************************************************
//*****************************************************************
