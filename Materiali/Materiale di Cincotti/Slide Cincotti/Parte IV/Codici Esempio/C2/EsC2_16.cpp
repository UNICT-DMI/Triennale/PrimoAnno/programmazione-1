//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC2_16.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Classi friend (I)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


//class Euro;

class Lira
  {
    private :

       int valore;

    public :

       Lira (int v)   { valore = v; }

       friend class Euro;

  }; //End class Lira


class Euro
  {
    private :

       int valore;

    public :

       Euro (int v)   { valore = v; }

       void Convertitore (float, Lira &);

  }; //End class Euro


void Euro::Convertitore (float coeff, Lira & l)
  {
    cout << valore * coeff << " lire  e  "
	 << l.valore / coeff << " euro\n";
  }


int main ()
  {
    Lira diecimila(10000);
    Euro dieci(10);

    cout << "10 euro  e  10.000 lire  equivalgono rispettivamente a\n";
    dieci.Convertitore (1936.27, diecimila);

   system("PAUSE");    // ns. comodo!
   return EXIT_SUCCESS;
} // End main()


//*****************************************************************
//*****************************************************************
