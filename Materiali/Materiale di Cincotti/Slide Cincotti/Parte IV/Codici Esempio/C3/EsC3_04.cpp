//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsC3_04.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Le classi
//***   Argomento      : Costruttori overloaded
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>
#include <string>

using namespace std;


class Complex
  {
    private :

       float Re;
       float Im;

    public:

       Complex ()
	  {
	    cout << "Costruttore di default\n";
	    Re = 0.0;
	    Im = 0.0;
	  }

       Complex (float a)
	  {
	    cout << "Costruttore con un parametro\n";
	    Re = a;
	    Im = 0.0;
	  }

       Complex (float a, float b)
	  {
	    cout << "Costruttore con due parametri\n";
	    Re = a;
	    Im = b;
	  }

  }; //End class Complex


int main ()
  {
    Complex x;
    Complex xx();            // Errore : prototipo di funzione
    Complex y(3.3);
    Complex W(2.7, 3.14);
    Complex z = Complex(2.0);         // Poco efficiente

    system("PAUSE");
    return EXIT_SUCCESS;
} // end main()


//*****************************************************************
//*****************************************************************
