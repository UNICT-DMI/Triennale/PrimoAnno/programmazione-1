//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : C4_03.cpp
//***   Data creazione : 05/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Funzionalita' basilari
//***   Argomento      : Overloading delle funzioni
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


// L'uso di parametri standard nelle funzioni
// overloaded puo' generare ambiguita'


double f (int, int = 10)
  {
    cout << "double\n";
    return 0.0l;
  }


float f (int)
  {
    cout << "float\n";
    return 0.0;
  }


int main()
  {
    f(2,4);

    // f(3);       // Genera ambiguita'

    // Eppure ...
    //
    //   int f (const int *);
    //   int f (int *);
    //
    //
    // Dichiarazione di un numero variabile di parametri ...
    //
    // int f (int, ...)
    //
   
    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }

//*****************************************************************
//*****************************************************************
