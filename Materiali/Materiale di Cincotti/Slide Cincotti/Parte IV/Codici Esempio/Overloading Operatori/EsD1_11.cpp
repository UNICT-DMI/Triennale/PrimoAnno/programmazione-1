//*****************************************************************
//*****************************************************************
//***
//***   Nome file      : EsD1_11.cpp
//***   Data creazione : 06/01/2014
//***   Progetto       : OOP in "C++"
//***   Lezione        : Overload di operatori
//***   Argomento      : Converione di tipo (III)
//***   -----------------------------------------------------------
//***   Autore         : G. Cincotti
//***   Organizzazione : Dipartimento di Matematica e Informatica
//***   E-mail         : cincotti@dmi.unict.it
//***
//*****************************************************************
//*****************************************************************


#include <iostream>

using namespace std;


class Messaggio
  {
    private :

       char * testo;
       char * mittente;
       int    priorita;

       void Set (const char *, const char *, int);

    public:

       Messaggio (const char *, const char * = "", int = 0);
       Messaggio (const Messaggio &);   // Costruttore di copia
       ~Messaggio ();

       const Messaggio &  operator = (const Messaggio &);
       Messaggio operator + (const Messaggio &);
       operator const char * () const;

       void Visualizza (void) const;

  }; //End class Messaggio


void Messaggio::Set (const char * t, const char * m, int p)
  {
    testo = new char [strlen(t)+1];
    mittente = new char [strlen(m)+1];
    strcpy (testo, t);
    strcpy (mittente, m);
    priorita = p;
  }


Messaggio::Messaggio (const Messaggio & mess)
  {
    Set (mess.testo, mess.mittente, mess.priorita);
    cout << "Costruttore di copia\n";
  }


Messaggio::Messaggio (const char * t, const char * m, int p)
  {
    Set (t,m,p);
  }


Messaggio::~Messaggio ()
  {
    delete [] testo;
    delete [] mittente;
  }


const Messaggio & Messaggio::operator = (const Messaggio & mess)
  {
    if (this != & mess)
       {
  	     this->~Messaggio ();
	     Set (mess.testo, mess.mittente, mess.priorita);
       }
    return *this;
  }


Messaggio Messaggio::operator + (const Messaggio & mess)
  {
    char * str = new char [strlen(mittente) + strlen(mess.mittente)+1];
    strcpy (str, mittente);
    strcat (str, mess.mittente);
    Messaggio aux("CC", str);
    delete [] str;
    return aux;
  }

Messaggio::operator const char * ()  const
  {
    cout << "overload cast\n";                
    return mittente;
  }


void Messaggio::Visualizza (void) const
  {
    cout << "Mitt.: " << mittente << "  Mess.: " << testo
	 << "  Liv. priorit… : " << priorita << '\n';
  }



int main ()
  {
    Messaggio a("Primo messaggio", "Pippo", 3);
    Messaggio b("Secondo messaggio", "Pluto", 1);
    Messaggio c = a;

    cout << (const char *) a << '\n';    // Conversione esplicita

    char str[100];
    strcpy (str, a);                     // Conversione implicita
    cout << str << "\n\n";

    c = a + b;
    cout << c << '\n';                   // Conversione implicita

    c = a + (Messaggio) "Altro messaggio";
    cout << c << '\n';                   // Conversione implicita

    system("PAUSE");    // ns. comodo!
    return EXIT_SUCCESS;
  }


//*****************************************************************
//*****************************************************************
