#include<iostream>
#include <string>

using namespace std;

//******************
class CarattereIII {
//******************
      
public:  //***************************

   void visualizzaUno (char c) { cout << c;}

   void visualizzaDieci (char c)
     { 
       int i=0;
       while (i++<10)    visualizzaUno(c);
     }
   
   void visualizzaN (char c, short n)
     { 
       int i=0;
       while (i<n)
          {         
            visualizzaUno(c);
            i++;
          }                 
     }
   
   void visualizzaRigheDa10 (char c, short r)
     { 
       int i=0;   
       while (i<r)
          {  
            visualizzaDieci(c);          
            cout << endl;
            i++;
          }         
	 }	
		
   void visualizzaRigheDaN (char c, short r, short n)
     { 
     int i=0;   
      while (i<r)
      {  
         visualizzaN(c,n);          
         cout << endl;
         i++;
      }            
     }
	

}; // End class CarattereIII

